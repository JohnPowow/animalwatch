Group: 11am Group 5  
Members: John Powers, Josh Brown, Sage Sanford, Alexander Han, Justin Leong  
Member EIDs: jcp3737, jgb2744, sjs4947, ayh358, jtl2593  
Member IDs: JohnPowow, jgbrown-ut, sanfordsage, alexanderyichenhan, justin_tl  

Project Leader for Phase 1: John Powers
 - Organize Meetings.
 - Checking in, making sure things get done

Project Leader for Phase 2: Alexander Han
 - Organize Meetings.
 - Handle deployment

 Project Leader for Phase 3: Sage Sanford
 - Organize Meetings.
 - Working and collaborating with backend/frontend to meet
 deadline goals.

  Project Leader for Phase 4: Joshua Brown
 - Organize meetings with frontend team to finish visualizations.
 - Create and make sure presentation finishes.

Project Name: AnimalWatch  

- Git SHA Phase 1: 50e7ce80b8f7f5805b656c25feb2ed689961b0c4
- Git SHA Phase 2: bcc26d67c6305d4b2ecfe52561eaf24aabbdfc87 
- Git SHA Phase 3: a268038f7522d859da3c561f89281ba36b15404a
- Git SHA Phase 4: 44f4917b9319fbf99539d39c882870e8a327dd3f

Gitlab Pipelines: https://gitlab.com/JohnPowow/animalwatch/-/pipelines  

Link to website: https://www.animalwatch.net

Estimated Time Phase 1
- John: 5hrs  
- Alex: 2hrs  
- Josh: 10hrs  
- Sage: 6hrs  
- Justin: 5hrs  

Actual Time Phase 1
- John: 12hrs  
- Alex: 15hrs  
- Josh: 7hrs  
- Sage: 8hrs  
- Justin: 7hrs  

Estimated Time Phase 2
- John: 15hrs  
- Alex: 15hrs  
- Josh: 25hrs  
- Sage: 20hrs  
- Justin: 12hrs  

Actual Time Phase 2
- John: 23hrs  
- Alex: 22hrs  
- Josh: 30hrs  
- Sage: 23hrs  
- Justin: 12hrs

Estimated Time Phase 3
- John: 10hrs  
- Alex: 16hrs  
- Josh: 14hrs  
- Sage: 10hrs  
- Justin: 8hrs  

Actual Time Phase 3
- John: 6hrs  
- Alex: 13hrs  
- Josh: 12hrs  
- Sage: 10hrs  
- Justin: 5hrs

Estimated Time Phase 4
- John: 5hrs  
- Alex: 2hrs  
- Josh: 2hrs  
- Sage: 4hrs  
- Justin: 2hrs  

Actual Time Phase 4
- John: 5hrs  
- Alex: 1hrs  
- Josh: 3hrs  
- Sage: 5hrs  
- Justin: 1hrs

Comments:
- For executing Selenium tests in the pipeline we referenced group Tuition Ball
  - https://gitlab.com/vijaykvuyyuru/swe-college-football-project/-/blob/dev/.gitlab-ci.yml
- For app.py we referenced group Texas Votes and group Music City
  - https://github.com/forbesye/texasvotes/blob/master/back-end/app.py
  - https://gitlab.com/m3263/musiccity/-/blob/develop/back-end/app/main.py
- For model instance retrieval we referenced group Chemicals Near Me
  - https://gitlab.com/cs373-11am-group5/chemicals-near-me/-/blob/develop/frontend/src/pages/Cities/city_instance_page.tsx
- For selenium test format we referenced group Texas Votes and group Drive Responsibly
  - https://gitlab.com/forbesye/fitsbits/-/blob/master/front-end/guitests.py
  - https://gitlab.com/ethanzh/drive-responsibly/-/blob/main/frontend/gui_tests/splashTests.py
- For pagination we referenced group Give & Live
  - https://gitlab.com/giveandlive/giveandlive/-/blob/main/front-end/src/pages/Pagination.js
- For dynamic About page we referenced group Pride in Writing and group Get That Bread
  - https://gitlab.com/Nathaniel-Nemenzo/getthatbread
  - https://gitlab.com/JunLum/pride-in-writing
- For highlighting, referenced Chemicals Near Me
  - https://gitlab.com/cs373-11am-group5/chemicals-near-me/-/blob/develop/frontend/src/pages/Cities/

GitLab URL: https://gitlab.com/JohnPowow/animalwatch
