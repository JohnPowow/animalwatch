.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

# All of these make commands must be called in root directory
all:

# auto format the code
format:
	black ./backend/*.py

install:
	pip install -r ./backend/requirements.txt

# check files, check their existence with make check
CFILES :=                                 \
    .gitignore                            \
    .gitlab-ci.yml                        
	
# check the existence of check files
check: $(CFILES)

# commands for frontend
install-frontend:
	cd frontend/ && npm install

build-frontend:
	cd frontend/ && npm build

run-frontend:
	cd frontend/ && npm start

format-frontend:
	cd frontend/ && npx prettier --write src

eslint-frontend:
	cd frontend/ && npx eslint --ext js,jsx,ts,tsx --fix src

selenium-tests:
	python frontend/gui_tests/runSeleniumTests.py

# remove temporary files
clean:
	rm -f  *.tmp
	rm -rf __pycache__
