import Homepage from './pages/Homepage.js';
import About from './pages/About.js';
import Parks from './pages/Parks.js';
import Recreation from './pages/Recreation.js';
import Species from './pages/Species.js';
import NavbarComp from './components/NavbarComp'
import SearchPage from './pages/SearchPage.js';

import SpeciesInstance from './pages/SpeciesInstance.js';
import RecreationInstance from './pages/RecreationInstance.js';
import ParkInstance from './pages/ParkInstance.js';

import OurVisualizations from './pages/OurVisualizations.js';
import ProviderVisualizations from './pages/ProviderVisualizations.js';

import 'bootstrap/dist/css/bootstrap.min.css';

import {
  BrowserRouter,
  Routes,
  Route,
  Navigate
} from "react-router-dom";



function App() {
  return (
    <BrowserRouter>
      <NavbarComp />
      <Routes>
        <Route path='/homepage' element={<Homepage />} />
        <Route path='/about' element={<About />} />
        <Route path='/parks' element={<Parks />} />
        <Route path='/recreation' element={<Recreation />} />
        <Route path='/species' element={<Species />} />
        <Route path='/searchpage' element={<SearchPage />} />
        <Route path='/our-visualizations' element={< OurVisualizations />} />
        <Route path='/provider-visualizations' element={< ProviderVisualizations />} />
        <Route path='/' element={<Navigate to='/Homepage' />} />

        <Route path='/parks/:id' element={<ParkInstance />} />
        <Route path='/species/:id' element={<SpeciesInstance />} />
        <Route path='/recreation/:id' element={<RecreationInstance />} />

      </Routes>
    </BrowserRouter>
  );
}

export default App;