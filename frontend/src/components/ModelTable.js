import Highlighter from "react-highlight-words";
import './ModelTable.css'


const ModelTable = ({ data, column, linkBase, searchTerms }) => {
  return (
    <table>
        <thead>
            <tr>
               {column.map((item, index) => <TableHeadItem item={item}/>)}
            </tr>
        </thead>
        <tbody>
            {data.map((item, index) => <TableRow item={item} column={column} linkBase={linkBase} searchTerms={searchTerms}/>)}
        </tbody>
    </table>
  )
}

const TableHeadItem = ({ item }) => <th>{item.heading}</th>

const TableRow = ({ item, column, linkBase, id, searchTerms}) => (

    <tr className='TableRow' onClick={() => window.location.href = linkBase + item['id']}>
        {column.map((columnItem, index) => {
            console.log(item[`${columnItem.value}`]);
            return <td>
                <Highlighter
                    searchWords={
                        searchTerms == null ? [] : searchTerms
                    }
                    textToHighlight={item[`${columnItem.value}`].toString()}
                />
                </td>
        })}
    </tr>
)

export default ModelTable