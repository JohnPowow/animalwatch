import React from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import './NavbarComp.css';
import AnimalWatch from '../assets/AnimalWatch.png';

const NavbarComp = () => {
    return (
        <div>
            <Navbar className="custom-bar" variant={"dark"} expand="lg" > 
                <Container>
                    <Navbar.Brand>
                        <img
                            src= {AnimalWatch}
                            width="60"
                            height="35"
                            className="d-inline-block align-top"
                            alt="React Bootstrap logo"
                        />{' '}
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="/Homepage"> Home </Nav.Link>
                            <Nav.Link href="/About">About</Nav.Link>
                            <Nav.Link href="/Species">Species</Nav.Link>
                            <Nav.Link href="/Parks">Parks</Nav.Link>
                            <Nav.Link href="/Recreation">Recreation</Nav.Link>
                            <Nav.Link href="/our-visualizations">Our Visualizations</Nav.Link>
                            <Nav.Link href="/provider-visualizations">Provider Visualizations</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </div>
    )
}

export default NavbarComp