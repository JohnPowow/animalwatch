import React from 'react'
import { Accordion, ListGroup } from 'react-bootstrap';

var eventNum = 0;

const InstanceAccordion = ({ listData, id }) => {

    console.log('Showing list data', listData)
    console.log('Showing keys ', Object.keys(listData))
  return (
    <Accordion>
        {Object.keys(listData).map((modelName, index) => <AccordionItem modelName={modelName} value={listData[modelName]} eventIteration={index} key={index}/>)}
    </Accordion>
  )
}

//iteration - figure out how to do.
const AccordionItem = ({ modelName, value, eventIteration }) => {
    console.log(eventIteration)
    return (
        <Accordion.Item eventKey={eventIteration}>
          <Accordion.Header>{modelName}</Accordion.Header> 
            <Accordion.Body>
              <ListGroup variant="flush">
                {value.map((nameInfo, index) => <AccordionGroups modelData={nameInfo} modelName={modelName} key={index}/>)}
              </ListGroup>
            </Accordion.Body> 
        </Accordion.Item>
    )
    
}

const AccordionGroups = ({ modelData, modelName,  }) => {
return (

<ListGroup.Item> <a href={'/' +  modelName + '/' +  modelData.id}> {modelData.name} </a> </ListGroup.Item>

)
}

export default InstanceAccordion