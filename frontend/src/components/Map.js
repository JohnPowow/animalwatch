import React from 'react'
import { useEffect, useState } from 'react';
import { GoogleMap, Marker, useJsApiLoader } from '@react-google-maps/api';

const Map = ({latitude, longitude, name}) => {

  const containerStyle = {
    width: '300px',
    height: '300px'
  };
  
  const center = {
    lat:  latitude,
    lng: longitude
  };
  

  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: "AIzaSyA_i-_jaC2RaDRTsxfAJ3l6jtQjkxdxmyY"
  })

  const [map, setMap] = useState(null);

  return isLoaded ? (
      <GoogleMap
        mapContainerStyle={containerStyle}
        center={center}
        zoom={10}
      >
        <Marker position={center} label={name}/>
      </GoogleMap>
  ) : <></>
}

export default Map