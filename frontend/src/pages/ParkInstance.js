import axios from 'axios';
import React from 'react'
import { useEffect, useState } from 'react';
import { Card } from 'react-bootstrap';
import { Table } from 'react-bootstrap';
import { useParams } from 'react-router-dom'
import InstanceAccordion from '../components/InstanceAccordion';
import './Instances.css';
import api_url from '../api';
import Map from '../components/Map';

const ParkInstance = () => {

  const { id } = useParams()

  const [parkData, setParkData] = useState([]);
  const [referenceData, setReferenceData] = useState({ species: [], recreation: [] });


  useEffect(() => {
    axios.get(api_url + '/parks/' + id) // find current page!
      .then(res => {
        setParkData(res.data);
        console.log("Response data", res.data)
        // plug into the table!
        var refData = {}

        if (res.data.species) {
          refData.species = res.data.species
        }
        if (res.data.recreation) {
          refData.recreation = res.data.recreation
        }
        setReferenceData(refData)
      })

      .catch(err => {
        console.log(err)
        //stop something?
      })
  }, [])

  useEffect(() => {
    console.log("Reference data updated", referenceData)
  }, [referenceData])


  return (
    <div className="info-box">
      <Card body className="main-card">
        <h3> {parkData.name} </h3>
        <div className="inner-flex">
          <div className="inner-card">
            <img
              src={parkData.image_1}
              className="padded-img"
            />
            {
              parkData.latitude ? <Map
                latitude={parseInt(parkData.latitude)}
                longitude={parseInt(parkData.longitude)}
                name={parkData.name}
              /> : <div>Loading</div>
            }

          </div>
        </div>
        <div className="inner-flex">
          <div className="inner-card">
            <p> {parkData.description}</p>
          </div>
        </div>
        <p>Number of AnimalWatch Species: {parkData.num_species}</p>
        <p>Admission Cost: {parkData.admission_cost}</p>
        <p>Latitude: {parkData.latitude}</p>
        <p>Longitude: {parkData.longitude}</p>
        <Table>
          <tr>
            <td>State(s)</td>
            <td>
              {parkData.states}
            </td>
          </tr>
          <tr>
            <td>Topics</td>
            <td>{parkData.topics}</td>
          </tr>
          <tr>
            <td>Activities</td>
            <td>{parkData.activities}</td>
          </tr>
          <tr>
            <td>Amenities</td>
            <td>{parkData.amenities}</td>
          </tr>
        </Table>


        <InstanceAccordion listData={referenceData} id={id} />



      </Card>
    </div>
  )
}

export default ParkInstance