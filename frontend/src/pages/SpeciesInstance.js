import axios from "axios";
import React from "react";
import { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import { Table } from "react-bootstrap";
import { useParams } from "react-router-dom";
import InstanceAccordion from "../components/InstanceAccordion";
import "./Instances.css";
import api_url from "../api";

const SpeciesInstance = () => {
  const { id } = useParams();

  const [speciesData, setSpeciesData] = useState([]);
  const [referenceData, setReferenceData] = useState({
    parks: [],
    recreation: [],
  });

  useEffect(() => {
    axios
      .get(api_url + "/species/" + id) // find current page!
      .then((res) => {
        setSpeciesData(res.data);
        console.log("Response data", res.data);
        // plug into the table!
        var refData = {};
        if (res.data.parks) {
          refData.parks = res.data.parks;
        }

        if (res.data.recreation) {
          refData.recreation = res.data.recreation;
        }

        setReferenceData(refData);
      })
      .catch((err) => {
        console.log(err);
        //stop something?
      });
  }, []);

  useEffect(() => {
    console.log("Reference data updated", referenceData);
  }, [referenceData]);

  return (
    <div className="info-box">
      <Card body className="main-card">
        <h3> {speciesData.name} </h3>
        <div className="inner-flex">
          <div className="inner-card">
            <img
              src={speciesData.image_1}
              className="padded-img"
            />
            <img
              src={speciesData.image_2}
              className="padded-img"

            />
          </div>
        </div>
        <div className="inner-flex">
          <div className="inner-card">
            <p> {speciesData.description}</p>
          </div>
        </div>
        <p>{speciesData.scientific_name}</p>
        <p>Other names: {speciesData.other_names}</p>
        <p>Informal Taxonomy: {speciesData.informal_taxonomy}</p>
        <p>Last update date: {speciesData.last_updated}</p>
        <Table>
          <tr>
            <td>State(s)</td>
            <td>{speciesData.states}</td>
          </tr>
          <tr>
            <td>Phylum</td>
            <td>{speciesData.tax_phylum}</td>
          </tr>
          <tr>
            <td>Class</td>
            <td>{speciesData.tax_class}</td>
          </tr>
          <tr>
            <td>US Conservation Status</td>
            <td>{speciesData.us_status}</td>
          </tr>
          <tr>
            <td>Global Conservation Status</td>
            <td>{speciesData.global_status}</td>
          </tr>
        </Table>

        <InstanceAccordion listData={referenceData} id={id} />
      </Card>
    </div>
  );
};

export default SpeciesInstance;
