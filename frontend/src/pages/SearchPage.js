import axios from "axios";
import React, { useEffect, useState } from "react";
import { Card, Form, Row, Col, Button } from "react-bootstrap";
import "./SearchPage.css";
import ModelTable from "../components/ModelTable";
import Paginate from "./Pagination";
import api_url from "../api";

// default instance numbers
var numInstancesSpecies = 288;
var numInstancesParks = 466;
var numInstancesRecreation = 649;

const SearchPage = () => {
  // Species //
  const [tableDataSpecies, setTableDataSpecies] = useState([]);
  const [currentPageSpecies, setCurrentPageSpecies] = useState(1);
  const [search, setSearch] = useState("");


  useEffect(() => {
    var params = {};
    if (search !== "") {
      params["search"] = search;
    }
    axios
      .get(api_url + "/species", { params })
      .then((res) => {
        numInstancesSpecies = res.data.count;
        setCurrentPageSpecies(1); // reset page back to one!
      })
      .catch((err) => {
        console.log(err);
        //stop something?
      });
  }, [search]); // needs to re run after filters and searches

  useEffect(() => {
    var params = {};
    params["page"] = currentPageSpecies;
    if (search !== "") {
      params["search"] = search;
    }

    axios
      .get(api_url + "/species", { params }) // find current page!
      .then((res) => {
        setTableDataSpecies(res.data.page);
        // plug into the table!
      })
      .catch((err) => {
        console.log(err);
        //stop something?
      });
  }, [currentPageSpecies, search]); // when changes happen, re run useEffect.

  const columnSpecies = [
    { heading: "Name", value: "name" },
    { heading: "Taxonomy", value: "informal_taxonomy" },
    { heading: "States", value: "states" },
    { heading: "US Conservation Status", value: "us_status" },
    { heading: "Global Conservation Status", value: "global_status" },
  ];

  // Parks //
  const [tableDataParks, setTableDataParks] = useState([]);
  const [currentPageParks, setCurrentPageParks] = useState(1);

  useEffect(() => {
    var params = {};

    if (search !== "") {
      params["search"] = search;
    }

    axios.get(api_url + '/parks', { params })
      .then(res => {
        numInstancesParks = res.data.count;
        setCurrentPageParks(1); // reset page back to one!
      })
      .catch(err => {
        console.log(err)
      })
  }, [search]) // needs to re run after filters and searches

  useEffect(() => {
    var params = {};
    params["page"] = currentPageParks;

    if (search !== "") {
      params["search"] = search;
    }
    axios.get(api_url + '/parks', { params }) // find current page!
      .then(res => {
        setTableDataParks(res.data.page)

        // plug into the table!
      })
      .catch(err => {
        console.log(err)
      })
  }, [currentPageParks, search]) // when changes happen, re run useEffect.

  const columnParks = [
    { heading: "Name", value: "name" },
    { heading: "Number of AnimalWatch Species", value: "num_species" },
    { heading: "States", value: "states" },
    { heading: "Admission Cost", value: "admission_cost" },
    { heading: "Latitude", value: "latitude" },
    { heading: "Longitude", value: "longitude" },
  ];

  // Recreation //
  const [tableDataRecreation, setTableDataRecreation] = useState([]);
  const [currentPageRecreation, setCurrentPageRecreation] = useState(1);

  useEffect(() => {
    var params = {};
    if (search !== "") {
      params["search"] = search;
    }
    axios.get(api_url + '/recreation', { params }) // find current page!
      .then(res => {
        numInstancesRecreation = res.data.count;
        setCurrentPageRecreation(1);
      })
      .catch(err => {
        console.log(err)
        //stop something?
      })
  }, [search]) // needs to re run after filters and searches

  useEffect(() => {
    var params = {};
    params["page"] = currentPageRecreation;

    if (search !== "") {
      params["search"] = search;
    }

    axios.get(api_url + '/recreation', { params }) // find current page!
      .then(res => {
        setTableDataRecreation(res.data.page)

        //fix pets allowed
      })
      .catch(err => {
        console.log(err)
        //stop something?
      })
  }, [currentPageRecreation, search]) // when changes happen to currentPage, re run useEffect.

  const columnRecreation = [
    { heading: "Name", value: "name" },
    { heading: "State", value: "state" },
    { heading: "Pets Allowed", value: "pets_allowed" },
    { heading: "Reservable", value: "reservable" },
    { heading: "Telephone", value: "telephone" },
    { heading: "Last Updated", value: "last_updated" },
    { heading: "Coordinates", value: "coordinates" },
  ];

  const onFormSubmit = e => { // wut am I doing?!?
    e.preventDefault();
    const formData = new FormData(e.target),
      formDataObj = Object.fromEntries(formData.entries())
  }

  function handleSubmit(event) {
    event.preventDefault();
  }

  return (
    <div className="info-box">
      <Card body className="main-card">
        <Form className="mx-3" onSubmit={onFormSubmit}>
          <Row className="my-3">
            <Col xs={0} />
            <Col>
              <Form.Group className="mb-3" controlId="searchBar">
                <Form.Label>Search for Information:</Form.Label>
                <Form.Control type="search" placeholder="Search" onChange={(e) => setSearch(e.target.value)} onSubmit={handleSubmit} />
              </Form.Group>
            </Col>
            <Col xs={0} />
          </Row>
        </Form>

        <div>
          <h3> Species </h3>
          <div className="inner-flex">
            <ModelTable
              data={tableDataSpecies}
              column={columnSpecies}
              linkBase={"/species/"}
              searchTerms={search.split(" ")}
            />
          </div>

          <div className="inner-flex">
            <Paginate
              totalItems={numInstancesSpecies}
              itemsPerPage={9}
              paginate={setCurrentPageSpecies}
            />
          </div>
        </div>

        <div>
          <h3> Parks </h3>
          <div className="inner-flex">
            <ModelTable
              data={tableDataParks}
              column={columnParks}
              linkBase={"/parks/"}
              searchTerms={search.split(" ")}
            />
          </div>

          <div className="inner-flex">
            <Paginate
              totalItems={numInstancesParks}
              itemsPerPage={9}
              paginate={setCurrentPageParks}
            />
          </div>
        </div>

        <div>
          <h3> Recreation </h3>
          <div className="inner-flex">
            <ModelTable
              data={tableDataRecreation}
              column={columnRecreation}
              linkBase={"/recreation/"}
              searchTerms={search.split(" ")}
            />
          </div>

          <div className="inner-flex">
            <Paginate
              totalItems={numInstancesRecreation}
              itemsPerPage={9}
              paginate={setCurrentPageRecreation}
            />
          </div>
        </div>
      </Card>
    </div>
  );
};

export default SearchPage;
