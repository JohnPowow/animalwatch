// Pagination code from group Give & Live and Just a Bill
// https://gitlab.com/giveandlive/giveandlive/-/blob/main/front-end/src/pages/Pagination.js
// https://gitlab.com/davidtrak/just-a-bill

import React from 'react'
import { Pagination } from 'react-bootstrap'
// import PropTypes from 'prop-types'
import { useState } from 'react'

const Paginate = ({ totalItems, itemsPerPage, paginate }) => {

    const [activePage, setActivePage] = useState(1)
    // Paginate prop is used so that the appropriate page will be fetched in the model pages.
    const updatePage = (num) => {
        setActivePage(num)
        paginate(num)
    }


    // How many on either side of active
    const span = Math.floor((itemsPerPage - 4) / 2);
    const lastPage = Math.ceil(totalItems / itemsPerPage);

    const prevButtonKey = "prevButton";
    const nextButtonKey = "nextButton";
    const leftEllipsisKey = "leftEllipsis";
    const rightEllipsisKey = "rightEllipsis";

    // Layout used when not enough pages to use ellipses based on maxSpan
    const displayAll = () => {
        let items = [];
        for (let i = 1; i <= lastPage; i++) {
            items.push(
                <Pagination.Item
                    key={i}
                    active={activePage === i}
                    onClick={() => {
                        if (activePage !== i) updatePage(i);
                    }}
                >
                    {i}
                </Pagination.Item>
            );
        }
        return items;
    };

    // Layout used when active page is not near edge
    // Ex.  "1 ... 4 5 '6' 7 8 ... 10"
    const activeInMiddle = () => {
        let items = [];
        items.push(
            <Pagination.Item
                key={1}
                onClick={() => {
                    updatePage(1);
                }}
            >
                {1}
            </Pagination.Item>,
            <Pagination.Ellipsis key={leftEllipsisKey} />
        );
        for (let i = activePage - span; i <= activePage + span; i++) {
            items.push(
                <Pagination.Item
                    key={i}
                    active={activePage === i}
                    onClick={() => {
                        if (activePage !== i) updatePage(i);
                    }}
                >
                    {i}
                </Pagination.Item>
            );
        }
        items.push(
            <Pagination.Ellipsis key={rightEllipsisKey} />,
            <Pagination.Item
                key={lastPage}
                onClick={() => {
                    updatePage(lastPage);
                }}
            >
                {lastPage}
            </Pagination.Item>
        );
        return items;
    };

    // Used when active is near edge
    // Ex.  "1 2 '3' ... 8 9 10"
    const activeOnEdge = () => {
        let items = [];
        for (let i = 1; i <= span + 2; i++) {
            items.push(
                <Pagination.Item
                    key={i}
                    active={activePage === i}
                    onClick={() => {
                        if (activePage !== i) updatePage(i);
                    }}
                >
                    {i}
                </Pagination.Item>
            );
        }
        items.push(<Pagination.Ellipsis key={leftEllipsisKey} />);
        for (let i = lastPage - span - 1; i <= lastPage; i++) {
            items.push(
                <Pagination.Item
                    key={i}
                    active={activePage === i}
                    onClick={() => {
                        if (activePage !== i) updatePage(i);
                    }}
                >
                    {i}
                </Pagination.Item>
            );
        }
        return items;
    };

    let content;
    if (lastPage - 1 + 1 <= itemsPerPage) {
        content = displayAll();
    } else if (
        activePage > 1 + span &&
        activePage < lastPage - span
    ) {
        content = activeInMiddle();
    } else {
        content = activeOnEdge();
    }

    return (
        <div className="page-control">
            <Pagination>
                <Pagination.Prev
                    key={prevButtonKey}
                    disabled={activePage === 1}
                    onClick={() => {
                        updatePage(activePage - 1);
                    }}
                />
                {content}
                <Pagination.Next
                    key={nextButtonKey}
                    disabled={activePage === lastPage}
                    onClick={() => {
                        updatePage(activePage + 1);
                    }}
                />
            </Pagination>
        </div>
    );
};

export default Paginate