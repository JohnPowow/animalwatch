import axios from "axios";
import React, { useEffect, useState } from "react";
import { Card } from "react-bootstrap";
import "./Parks.css";
import ModelTable from "../components/ModelTable";
import Paginate from "./Pagination";
import { Form, Row, Col, Button } from "react-bootstrap";
import api_url from "../api";

const Species = () => {
  const [numInstances, setNumInstances] = useState(0);

  const [tableData, setTableData] = useState([]);

  const [currentPage, setCurrentPage] = useState(1);

  const [taxonomyClass, setTaxnomyClass] = useState("");

  const [taxonomyPhylum, setTaxonomyPhylum] = useState("");

  const [usState, setusState] = useState("");

  const [globalStatus, setGlobalStatus] = useState("");

  const [sort, setSort] = useState("");

  const [search, setSearch] = useState("");

  useEffect(() => {
    var params = {};
    if (taxonomyClass !== "") {
      params["tax_class"] = taxonomyClass;
    }
    if (taxonomyPhylum !== "") {
      params["tax_phylum"] = taxonomyPhylum;
    }
    if (globalStatus !== "") {
      params["global_status"] = globalStatus;
    }
    if (usState !== "") {
      params["states"] = usState;
    }
    if (search !== "") {
      params["search"] = search;
    }

    console.log(params);
    axios
      .get(api_url + "/species", { params })
      .then((res) => {
        setNumInstances(res.data.count);
        setCurrentPage(1); // reset page back to one!
      })
      .catch((err) => {
        console.log(err);
        //stop something?
      });
  }, [taxonomyClass, taxonomyPhylum, globalStatus, usState, search]); // needs to re run after filters and searches

  useEffect(() => {
    var params = {};
    params["page"] = currentPage;
    if (taxonomyClass !== "") {
      params["tax_class"] = taxonomyClass;
    }
    if (taxonomyPhylum !== "") {
      params["tax_phylum"] = taxonomyPhylum;
    }
    if (globalStatus !== "") {
      params["global_status"] = globalStatus;
    }
    if (usState !== "") {
      params["states"] = usState;
    }
    if (sort !== "") {
      params["sort"] = sort;
    }
    if (search !== "") {
      params["search"] = search;
    }

    axios
      .get(api_url + "/species", { params }) // find current page!
      .then((res) => {
        setTableData(res.data.page);
        console.log(tableData);

        // plug into the table!
      })
      .catch((err) => {
        console.log(err);
        //stop something?
      });
  }, [
    currentPage,
    taxonomyClass,
    taxonomyPhylum,
    globalStatus,
    usState,
    sort,
    search,
  ]); // when changes happen, re run useEffect.

  const column = [
    { heading: "Name", value: "name" },
    // { heading: "Taxonomy", value: "informal_taxonomy" },
    { heading: "Taxonomy Class", value: "tax_class" },
    { heading: "Taxonomy Phylum", value: "tax_phylum" },
    { heading: "States", value: "states" },
    { heading: "US Conservation Status", value: "us_status" },
    { heading: "Global Conservation Status", value: "global_status" },
  ];

  const onFormSubmit = (e) => {
    // wut am I doing?!?
    e.preventDefault();
    const formData = new FormData(e.target),
      formDataObj = Object.fromEntries(formData.entries());
  };

  function handleSubmit(event) {
    event.preventDefault();
  }

  return (
    <div className="info-box">
      <Card body className="main-card">
        <h3> Species </h3>
        <h5> Instances: {numInstances} </h5>

        <Form className="mx-4" onSubmit={onFormSubmit}>
          <Row className="mt-3">
            <Form.Group className="mb-3" controlId="searchBar">
              <Form.Label>Search</Form.Label>
              <Form.Control
                type="search"
                placeholder="Search"
                onChange={(e) => setSearch(e.target.value)}
                onSubmit={handleSubmit}
              />
            </Form.Group>
          </Row>

          <Row className="mb-3">
            <Form.Group as={Col} controlId="formTaxClass">
              <Form.Label>Taxonomy Class</Form.Label>
              <Form.Select
                type="class"
                onChange={(e) => setTaxnomyClass(e.target.value)}
              >
                <option value="">-</option>
                <option value="Actinopterygii">Actinopterygii</option>
                <option value="Amphibia">Amphibia</option>
                <option value="Aves">Aves</option>
                <option value="Bivalvia">Bivalvia</option>
                <option value="Branchiopoda">Branchiopoda</option>
                <option value="Chelonia">Chelonia</option>
                <option value="Crocodylia">Crocodylia</option>
                <option value="Gastropoda">Gastropoda</option>
                <option value="Insecta">Insecta</option>
                <option value="Malacostraca">Malacostraca</option>
                <option value="Mammalia">Mammalia</option>
                <option value="Reptilia">Reptilia</option>
              </Form.Select>
            </Form.Group>

            <Form.Group as={Col} controlId="formTaxPhylum">
              <Form.Label>Taxonomy Phylum</Form.Label>
              <Form.Select
                type="phylum"
                onChange={(e) => setTaxonomyPhylum(e.target.value)}
              >
                <option value="">-</option>
                <option value="Arthropoda">Arthropoda</option>
                <option value="Craniata">Craniata</option>
                <option value="Mollusca">Mollusca</option>
              </Form.Select>
            </Form.Group>
          </Row>

          <Row className="mb-3">
            <Form.Group as={Col} controlId="formGlobalStatus">
              <Form.Label>Global Conservation Status</Form.Label>
              <Form.Select
                type="status"
                onChange={(e) => setGlobalStatus(e.target.value)}
              >
                <option value="">-</option>
                <option value="G1">G1</option>
                <option value="G2">G2</option>
                <option value="G3">G3</option>
                <option value="G4">G4</option>
                <option value="G5">G5</option>
                <option value="GX">GX</option>
                <option value="GH">GH</option>
                <option value="T1">T1</option>
                <option value="T2">T2</option>
                <option value="T3">T3</option>
                <option value="T4">T4</option>
                <option value="TX">TX</option>
                <option value="TH">TH</option>
              </Form.Select>
            </Form.Group>

            <Form.Group as={Col} controlId="formState">
              <Form.Label>State</Form.Label>
              <Form.Select
                type="state"
                onChange={(e) => setusState(e.target.value)}
              >
                <option value="">-</option>
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>
              </Form.Select>
            </Form.Group>
          </Row>

          <Form.Group as={Col} controlId="formSort">
            <Form.Label>Sort by</Form.Label>
            <Form.Select type="sort" onChange={(e) => setSort(e.target.value)}>
              <option value="">-</option>
              <option value="name">Name</option>
              <option value="tax_class">Taxonomy Class</option>
              <option value="tax_phylum">Taxonomy Phylum</option>
              <option value="global_status">Global Conservation Status</option>
            </Form.Select>
          </Form.Group>
        </Form>

        {/* searchTerms={search} */}
        <div className="inner-flex">
          <ModelTable
            data={tableData}
            column={column}
            linkBase={"/species/"}
            searchTerms={search.split(" ")}
          />
        </div>

        <div className="inner-flex">
          <Paginate
            totalItems={numInstances}
            itemsPerPage={9}
            paginate={setCurrentPage}
          />
        </div>
      </Card>
    </div>
  );
};

export default Species;
