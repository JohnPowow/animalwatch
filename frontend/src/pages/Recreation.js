import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Card } from 'react-bootstrap';
import './Parks.css'
import { Form, Row, Col, Button } from "react-bootstrap";
import ModelTable from '../components/ModelTable';
import Paginate from './Pagination';
import api_url from '../api';


const Recreation = () => {
  const [numInstances, setNumInstances] = useState(0);

  const [tableData, setTableData] = useState([]);

  const [currentPage, setCurrentPage] = useState(1);

  const [petsAllowed, setPetsAllowed] = useState("");

  const [reservable, setReservable] = useState("");

  const [usState, setusState] = useState("");

  const [sort, setSort] = useState("");

  const [search, setSearch] = useState("");

  useEffect(() => {
    var params = {};
    if (petsAllowed !== "") {
      params["pets_allowed"] = petsAllowed;
    }
    if (reservable !== "") {
      params["reservable"] = reservable;
    }
    if (sort !== "") {
      params["sort"] = sort;
    }
    if (usState !== "") {
      params["state"] = usState;
    }
    if (search !== "") {
      params["search"] = search;
    }
    axios.get(api_url + '/recreation', { params }) // find current page!
      .then(res => {
        console.log(res.data.count)
        setNumInstances(res.data.count);
        setCurrentPage(1); // reset page back to one!
      })
      .catch(err => {
        console.log(err)
        //stop something?
      })
  }, [petsAllowed, reservable, usState, search]) // needs to re run after filters and searches

  useEffect(() => {
    var params = {};
    params["page"] = currentPage;
    if (petsAllowed !== "") {
      params["pets_allowed"] = petsAllowed;
    }
    if (reservable !== "") {
      params["reservable"] = reservable;
    }
    if (sort !== "") {
      params["sort"] = sort;
    }
    if (usState !== "") {
      params["state"] = usState;
    }
    if (search !== "") {
      params["search"] = search;
    }
    axios.get(api_url + '/recreation', { params }) // find current page!
      .then(res => {
        setTableData(res.data.page)
        console.log(tableData)

        //fix pets allowed
      })
      .catch(err => {
        console.log(err)
        //stop something?
      })
  }, [currentPage, petsAllowed, reservable, usState, search, sort]) // when changes happen to currentPage, re run useEffect.

  const column = [
    { heading: 'Name', value: 'name' },
    { heading: 'State', value: 'state' },
    { heading: 'Pets Allowed', value: 'pets_allowed' },
    { heading: 'Reservable', value: 'reservable' },
    { heading: 'Telephone', value: 'telephone' },
    { heading: 'Last Updated', value: 'last_updated' },
    { heading: 'Coordinates', value: 'coordinates' }
  ]

  const onFormSubmit = e => { // wut am I doing?!?
    e.preventDefault();
    const formData = new FormData(e.target),
      formDataObj = Object.fromEntries(formData.entries())
  }

  function handleSubmit(event) {
    event.preventDefault();
  }

  return (
    <div className='info-box'>
      <Card body className='main-card'>
        <h3> Recreation </h3>
        <h5> Instances: {numInstances} </h5>

        <Form className="mx-4" onSubmit={onFormSubmit}>
          <Row className="mt-3">
            <Form.Group className="mb-3" controlId="searchBar">
              <Form.Label>Search</Form.Label>
              <Form.Control type="search" placeholder="Search" onChange={(e) => setSearch(e.target.value)} onSubmit={handleSubmit} />
            </Form.Group>
          </Row>

          <Row className="mb-3">
            <Form.Group as={Col} controlId="formPetsAllowed">
              <Form.Label>Pets Allowed</Form.Label>
              <Form.Select type="phylum" onChange={(e) => setPetsAllowed(e.target.value)}>
                <option value="">-</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </Form.Select>
            </Form.Group>

            <Form.Group as={Col} controlId="formReservable">
              <Form.Label>Reservable</Form.Label>
              <Form.Select type="phylum" onChange={(e) => setReservable(e.target.value)}>
                <option value="">-</option>
                <option value="Yes">Yes</option>
                <option value="No">No</option>
              </Form.Select>
            </Form.Group>
          </Row>

          <Row className="mb-3">
            <Form.Group as={Col} controlId="formState">
              <Form.Label>State</Form.Label>
              <Form.Select type="state" onChange={(e) => setusState(e.target.value)}>
                <option value="">-</option>
                <option value="AL">Alabama</option>
                <option value="AK">Alaska</option>
                <option value="AZ">Arizona</option>
                <option value="AR">Arkansas</option>
                <option value="CA">California</option>
                <option value="CO">Colorado</option>
                <option value="CT">Connecticut</option>
                <option value="DE">Delaware</option>
                <option value="FL">Florida</option>
                <option value="GA">Georgia</option>
                <option value="HI">Hawaii</option>
                <option value="ID">Idaho</option>
                <option value="IL">Illinois</option>
                <option value="IN">Indiana</option>
                <option value="IA">Iowa</option>
                <option value="KS">Kansas</option>
                <option value="KY">Kentucky</option>
                <option value="LA">Louisiana</option>
                <option value="ME">Maine</option>
                <option value="MD">Maryland</option>
                <option value="MA">Massachusetts</option>
                <option value="MI">Michigan</option>
                <option value="MN">Minnesota</option>
                <option value="MS">Mississippi</option>
                <option value="MO">Missouri</option>
                <option value="MT">Montana</option>
                <option value="NE">Nebraska</option>
                <option value="NV">Nevada</option>
                <option value="NH">New Hampshire</option>
                <option value="NJ">New Jersey</option>
                <option value="NM">New Mexico</option>
                <option value="NY">New York</option>
                <option value="NC">North Carolina</option>
                <option value="ND">North Dakota</option>
                <option value="OH">Ohio</option>
                <option value="OK">Oklahoma</option>
                <option value="OR">Oregon</option>
                <option value="PA">Pennsylvania</option>
                <option value="RI">Rhode Island</option>
                <option value="SC">South Carolina</option>
                <option value="SD">South Dakota</option>
                <option value="TN">Tennessee</option>
                <option value="TX">Texas</option>
                <option value="UT">Utah</option>
                <option value="VT">Vermont</option>
                <option value="VA">Virginia</option>
                <option value="WA">Washington</option>
                <option value="WV">West Virginia</option>
                <option value="WI">Wisconsin</option>
                <option value="WY">Wyoming</option>
              </Form.Select>
            </Form.Group>

            <Form.Group as={Col} controlId="formSort">
              <Form.Label>Sort by</Form.Label>
              <Form.Select type="sort" onChange={(e) => setSort(e.target.value)}>
                <option value="">-</option>
                <option value="name">Name</option>
                <option value="last_updated">Last Updated</option>
              </Form.Select>
            </Form.Group>
          </Row>
        </Form>

        <div className='inner-flex'>
          <ModelTable data={tableData} column={column} linkBase={'/recreation/'} searchTerms={search.split(" ")} />
        </div>

        <div className='inner-flex'>
          <Paginate totalItems={numInstances} itemsPerPage={9} paginate={setCurrentPage} />
        </div>
      </Card>
    </div>
  )
}

export default Recreation