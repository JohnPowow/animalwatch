import React, { useEffect, useState } from 'react';
import axios from "axios";
import { Area, AreaChart, BarChart, Bar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, Radar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Label } from 'recharts';

const ProviderVisualizations = () => {

  const [rData, setRData] = useState([]);
  var rDict = {}

  useEffect(() => {
    axios.get(`https://api.giveandlive.me/charity?ratings`)
      .then(res => {

        //for each member, find them in the array and their name from the api call and update their commits accordingly
        res.data.content.forEach(charity => {
          var r = charity['overallRating']
          console.log(r)

          if (r in rDict) {
            rDict[r] += 1
          } else {
            rDict[r] = 1
          }
        })

        console.log(rDict)

        const keys = Object.keys(rDict)

        keys.forEach((key, index) => {

          var newEntry = {
            rating: key,
            value: rDict[key],
          }

          setRData(oldData => [...oldData, newEntry])

        })
      })

      .catch(err => {
        console.log(err)
      })
  }, [])

  const [gData, setGData] = useState([]);
  var gen_dict = {}

  useEffect(() => {

    axios.get(`https://api.giveandlive.me/event?genres`)
      .then(res => {

        res.data.content.forEach(event => {
          var gen = event['genre']
          if (gen !== "undefined" && gen !== "NaN" && gen !== "Undefined" && gen !== "Miscellaneous" && gen !== "Other") {
            if (gen in gen_dict) {
              gen_dict[gen] = gen_dict[gen] + 1
            } else if (gen !== 0) {
              gen_dict[gen] = 1
            }
          }
        })

        const keys = Object.keys(gen_dict)

        keys.forEach((key, index) => {

          var newEntry = {
            genre: key,
            value: gen_dict[key],
          }

          setGData(oldData => [...oldData, newEntry])

        })
      })
      .catch(err => {
        console.log(err)
      })
  }, []) // needs to re run after filters and searches



  const [sData, setSData] = useState([]);
  var s_dict = {}

  useEffect(() => {
    axios.get(`https://api.giveandlive.me/location?safeties`) // find current page!
      .then(res => {

        res.data.content.forEach(location => {
          var s = location['safety']
          if (s in s_dict) {
            s_dict[s] = s_dict[s] + 1
          } else {
            s_dict[s] = 1
          }
        })

        const keys = Object.keys(s_dict)

        keys.forEach((key, index) => {

          var newEntry = {
            safety: key,
            value: s_dict[key],
          }

          setSData(oldData => [...oldData, newEntry])

        })
      })
      .catch(err => {
        console.log(err)
        //stop something?
      })
  }, []) // needs to re run after filters and searches

  return (
    <div>
      <ResponsiveContainer width={'99%'} height={300}>
        <BarChart width={500} height={300} data={rData} margin={{
          top: 5,
          right: 30,
          left: 20,
          bottom: 5,
        }}>
          <XAxis dataKey="rating">
            <Label value="Charity Rating" offset={-5} position="insideBottom" />
          </XAxis>
          <YAxis>
            <Label value="Number of Charities" offset={20} position="insideBottomLeft" angle="-90" />
          </YAxis>

          <Tooltip />
          <Bar dataKey="value" fill="#d685ff" />
        </BarChart>
      </ResponsiveContainer>

      <ResponsiveContainer width={'99%'} height={300}>
        <AreaChart
          width={500} height={300} data={gData} margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5,
          }}>
          <CartesianGrid strokeDasharray="3 3" />
          <XAxis dataKey="genre">
            <Label value="Genres" offset={-1} position="insideBottom" />
          </XAxis>
          <YAxis>
            <Label value="Number of Events" offset={20} position="insideBottomLeft" angle="-90" />
          </YAxis>
          <Tooltip />
          <Area dataKey="value" name="Total Instances" stroke="#ffc7fd" fill="#ffc7fc" />
        </AreaChart>
      </ResponsiveContainer>

      <ResponsiveContainer width={'99%'} height={1000}>
        <RadarChart cx="50%" cy="50%" outerRadius="80%" data={sData}>
          <PolarGrid />
          <PolarAngleAxis dataKey="safety" />
          <PolarRadiusAxis angle={30} domain={[0, 50]}>
            <Label value="Frequency of Safety Ratings" offset={50} position="center" />
          </PolarRadiusAxis>
          <Radar dataKey="value" stroke="#a6edaa" fill="#a6edaa" fillOpacity={0.6} />
          <Legend />
        </RadarChart>
      </ResponsiveContainer>
    </div>
  );
}

export default ProviderVisualizations