import React, { useEffect, useState } from 'react';
import axios from "axios";
import { Area, AreaChart, BarChart, Bar, RadarChart, PolarGrid, PolarAngleAxis, PolarRadiusAxis, Radar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, Label } from 'recharts';
import api_url from "../api";

const OurVisualizations = () => {

    const [sData, setSData] = useState([]);
    var gs_dict = {
        "G1": 0,
        "G2": 0,
        "G3": 0,
        "G4": 0,
        "G5": 0,
        "GX": 0,
        "GH": 0,
        "T1": 0,
        "T2": 0,
        "T3": 0,
        "T4": 0,
        "TX": 0,
        "TH": 0
    }

    useEffect(() => {
        axios.get(api_url + '/species')
            .then(res => {

                //for each member, find them in the array and their name from the api call and update their commits accordingly.

                res.data.page.forEach(species => {
                    var gs = species['global_status']
                    gs_dict[gs] = gs_dict[gs] + 1
                })

                const keys = Object.keys(gs_dict)

                keys.forEach((key, index) => {

                    var newEntry = {
                        name: key,
                        value: gs_dict[key],
                    }

                    setSData(oldData => [...oldData, newEntry])

                })
            })

            .catch(err => {
                console.log(err)
            })
    }, [])


    const [pData, setPData] = useState([]);
    var parks_dict = {}

    useEffect(() => {

        axios.get(api_url + '/parks')
            .then(res => {

                res.data.page.forEach(parks => {
                    var num = parks['num_species']
                    if (num in parks_dict) {
                        parks_dict[num] = parks_dict[num] + 1
                    } else if (num !== 0) {
                        parks_dict[num] = 1
                    }
                })

                const keys = Object.keys(parks_dict)

                keys.forEach((key, index) => {

                    var newEntry = {
                        park: key,
                        value: parks_dict[key],
                    }

                    setPData(oldData => [...oldData, newEntry])

                })
            })
            .catch(err => {
                console.log(err)
            })
    }, []) // needs to re run after filters and searches



    const [rData, setRData] = useState([]);
    var s_dict = {}

    useEffect(() => {
        axios.get(api_url + '/recreation') // find current page!
            .then(res => {

                res.data.page.forEach(recreation => {
                    var s = recreation['state']
                    if (s !== "not available" && s !== "") {
                        if (s === "OREGON") {
                            s = "OR"
                        } else if (s === "Tennessee") {
                            s = "TN"
                        }
                        if (s in s_dict) {
                            s_dict[s] = s_dict[s] + 1
                        } else {
                            s_dict[s] = 1
                        }
                    }
                })

                const keys = Object.keys(s_dict)
                console.log(keys)

                keys.forEach((key, index) => {

                    var newEntry = {
                        state: key,
                        value: s_dict[key],
                    }

                    setRData(oldData => [...oldData, newEntry])

                })
            })
            .catch(err => {
                console.log(err)
                //stop something?
            })
    }, []) // needs to re run after filters and searches

    return (
        <div>
            <ResponsiveContainer width={'99%'} height={300}>
                <BarChart width={500} height={300} data={sData} margin={{
                    top: 5,
                    right: 30,
                    left: 20,
                    bottom: 5,
                }}>
                    <XAxis dataKey="name">
                        <Label value="Global Conservation Status" offset={-5} position="insideBottom" />
                    </XAxis>
                    <YAxis>
                        <Label value="Number of Species" offset={20} position="insideBottomLeft" angle="-90" />
                    </YAxis>

                    <Tooltip />
                    <Bar dataKey="value" fill="#ffc180" />
                </BarChart>
            </ResponsiveContainer>




            <ResponsiveContainer width={'99%'} height={300}>
                <AreaChart
                    width={500} height={300} data={pData} margin={{
                        top: 5,
                        right: 30,
                        left: 20,
                        bottom: 5,
                    }}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="park">
                        <Label value="Number of Species in a Single Park" offset={-1} position="insideBottom" />
                    </XAxis>
                    <YAxis>
                        <Label value="Number of Parks" offset={20} position="insideBottomLeft" angle="-90" />
                    </YAxis>
                    <Tooltip />
                    <Area dataKey="value" stroke="#82f7ff" fill="#82f7ff" />
                </AreaChart>
            </ResponsiveContainer>

            <ResponsiveContainer width={'99%'} height={1000}>
                <RadarChart cx="50%" cy="50%" outerRadius="80%" data={rData}>
                    <PolarGrid />
                    <PolarAngleAxis dataKey="state" />
                    <PolarRadiusAxis angle={30} domain={[0, 50]}>
                        <Label value="Frequency of Recreation Locations per State" offset={0} position="center" />
                    </PolarRadiusAxis>
                    <Radar dataKey="value" stroke="#ff6183" fill="#ff6183" fillOpacity={0.6} />
                    <Legend />
                </RadarChart>
            </ResponsiveContainer>

        </div>
    );
}

export default OurVisualizations