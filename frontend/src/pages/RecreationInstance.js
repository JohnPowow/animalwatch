import axios from 'axios';
import React from 'react'
import { useEffect, useState } from 'react';
import { Card } from 'react-bootstrap';
import { Table } from 'react-bootstrap';
import { useParams } from 'react-router-dom'
import InstanceAccordion from '../components/InstanceAccordion';
import api_url from '../api';
import Map from '../components/Map';
import './Instances.css';

const RecreationInstance = () => {

  const { id } = useParams()

  const [recreationData, setRecreationData] = useState([]);
  const [referenceData, setReferenceData] = useState({ parks: [], species: [] });


  useEffect(() => {
    axios.get(api_url + '/recreation/' + id) // find current page!
      .then(res => {
        setRecreationData(res.data);
        console.log("Response data", res.data)
        // plug into the table!
        var refData = {}
        if (res.data.parks) {
          refData.parks = res.data.parks
        }

        if (res.data.species) {
          refData.species = res.data.species
        }

        setReferenceData(refData)


      })
      .catch(err => {
        console.log(err)
        //stop something?
      })
  }, [])

  useEffect(() => {
    console.log("Reference data updated", referenceData)
  }, [referenceData])

  return (
    <div className="info-box">
      <Card body className="main-card">
        <h3> {recreationData.name} </h3>
        <div className="inner-flex">
          <div className="inner-card">
            <p></p>
          </div>
        </div>
        <div className="inner-flex">
          <div className="inner-card">
            <img
              src={recreationData.image_1}
              className="padded-img"
            />
            {
              recreationData.coordinates ? <Map
                latitude={parseInt(recreationData.coordinates.split(", ")[1])}
                longitude={parseInt(recreationData.coordinates.split(", ")[0])}
                name={recreationData.name}
              /> : <div>Loading</div>
            }
          </div>
        </div>
        <p> {recreationData.description}</p>
        <p>Link: <a href={recreationData.link}> {recreationData.link} </a></p>
        <p>Telephone: {recreationData.telephone}</p>
        <p>Activities: {recreationData.activities}</p>
        <p>Last update date: {recreationData.last_updated}</p>
        <Table>
          <tr>
            <td>State</td>
            <td>
              {recreationData.state}
            </td>
          </tr>
          <tr>
            <td>Coordinates</td>
            <td>{recreationData.coordinates}</td>
          </tr>
          <tr>
            <td>Reservable</td>
            <td>{recreationData.reservable}</td>
          </tr>
          <tr>
            <td>Pets Allowed</td>
            <td>{recreationData.pets_allowed}</td>
          </tr>
          <tr>
            <td>Mandatory Equipment</td>
            <td>{recreationData.mandatory_equipment}</td>
          </tr>
        </Table>


        <InstanceAccordion listData={referenceData} id={id} />



      </Card>
    </div>
  )
}

export default RecreationInstance