import React from 'react';
import './Homepage.css';
import { Container, Card, Row, Col, Button } from "react-bootstrap";

const models = {
  entry: [
    {
      name: "Species",
      img: "catpic.png",
      link: "/species",
      description: "Learn about species and where you can find them in the wild!"
    },
    {
      name: "Parks",
      img: "park.png",
      link: "/parks",
      description: "Discover the many national parks you can explore across the country."
    },
    {
      name: "Recreation",
      img: "camp.png",
      link: "/recreation",
      description: "Find recreational areas to reserve for your next adventure!"
    },
  ],
};


const Homepage = () => {
  return (
    <div className='main-div'>

      <div className='home-image' style={{ backgroundImage: `url("photobar.jpg")` }}>  </div>

      <div className='text-place'>
        <p>Welcome to Animalwatch. Let's get watching.</p>
      </div>

      <div className='button-place'>
        <Button href="/searchpage" variant="success">Search the Page</Button>
      </div>

      <div className='bottom-div'>
        <Container>
          <Row md={3}>
            {models.entry.map((item) => (
              <Col>
                <Card className="text-center" mt-3>
                  <Card.Img class="card-img" variant="top" src={item.img} />
                  <Card.Body>
                    <Card.Title>{item.name}</Card.Title>
                    {item.description}
                  </Card.Body>
                  <a href={item.link} className="stretched-link"></a>
                </Card>
              </Col>
            ))}
          </Row>
        </Container>
      </div>
    </div>
  )
}

export default Homepage