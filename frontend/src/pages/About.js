import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Card } from 'react-bootstrap';
import './About.css'

// Took code/inspiration from Pride in Writing and from GetThatBread
// Links: https://gitlab.com/Nathaniel-Nemenzo/getthatbread
// https://gitlab.com/JunLum/pride-in-writing

var totalIssues = 0;
var totalCommits = 0;
var totalTests = 80;


const About = () => {

    const [alexState, alexUpdate] = useState({ name: 'Alexander', username: 'alexanderyichenhan', commits: 0, tests: 0, issues: 0 });
    const [johnState, johnUpdate] = useState({ name: 'John', username: 'JohnPowow', commits: 0, tests: 0, issues: 0 });
    const [joshuaState, joshuaUpdate] = useState({ name: 'Joshua', username: 'jgbrown-ut', commits: 0, tests: 0, issues: 0 });
    const [justinState, justinUpdate] = useState({ name: 'Justin', username: 'jtl2593', commits: 0, tests: 0, issues: 0 });
    const [sageState, sageUpdate] = useState({ name: 'Sage', username: 'sanfordsage', commits: 0, tests: 0, issues: 0 });

    const personArray = [[alexState, alexUpdate], [johnState, johnUpdate], [joshuaState, joshuaUpdate], [joshuaState, joshuaUpdate], [justinState, justinUpdate], [sageState, sageUpdate]]

    //componentDidMount for Hooks
    useEffect(() => {

        //getting commit data
        axios.get('https://gitlab.com/api/v4/projects/33892263/repository/contributors')
            .then(res => {

                //for each member, find them in the array and their name from the api call and update their commits accordingly.
                res.data.forEach(member => {
                    var firstName = member['name'].split(' ')[0]; //gets the First nam
                    var index = findMember(firstName);
                    if (index != -1) { //found member, adding to their commits.
                        personArray[index][1]({ ...personArray[index][0], commits: personArray[index][0].commits += member['commits'] });
                        totalCommits += member['commits'];
                    }
                })
            }
            )

        //getting issues data
        personArray.forEach((member, index) => {
            axios.get('https://gitlab.com/api/v4/projects/33892263/issues_statistics?assignee_username=' + member[0]['username'])
                .then(res => { //getting call based on their username, then adding to their issues state.
                    var issuesData = res.data;
                    personArray[index][1]({ ...personArray[index][0], issues: personArray[index][0].issues += issuesData['statistics']['counts']['closed'] });
                    totalIssues += issuesData['statistics']['counts']['closed'];
                })
        })
    }, []); //second argument here so it only runs once (avoids infinite loop)


    // returns index of the new member for api call.
    const findMember = (firstName) => {
        for (var index = 0; index < personArray.length; index++) {
            var member = personArray[index][0];
            if (firstName === member['name'] || firstName === member['username']) {
                return index;
            }
        }
        return -1;
    }

    return (
        <div className='info-about-box'>

            {/* FIRST CARD */}
            <Card body className='main-about-card'>
                <h3> About Us </h3>
                <p> Animalwatch is a website for locating specific animals in national parks, and discovering the parks' campgrounds and recreational activities. This would allow the user to plan trips to observe animals across the United States.</p>
                <p> With this data, we hope to inspire people to explore the great outdoors and be more in touch with what the earth has to offer. </p>
            </Card>

            {/* SECOND CARD */}
            <Card body className='main-about-card'>
                <h3>Gitlab Status</h3>
                <p>Total Commits: {totalCommits} </p>
                <p>Total Issues: {totalIssues}</p>
                <p>Total unit tests: {totalTests}</p>
            </Card>

            {/* THIRD CARD */}
            <Card body className='main-about-card'>
                <h3> The Team </h3>

                <div className='inner-about-flex'>
                    <Card className='inner-about-card'>
                        <h3> Alexander Han </h3>

                        <img
                            src="alexpfp.png"
                            float="left"
                            width="50%"
                            height="50%"
                            backgroundSize="cover"
                            alt="picture of alex"
                        />

                        <p> Alex is a Senior at UT Austin studying Computer Science and will be graduating in May 2022. His main role was back-end. </p>
                        <p> Number of Commits: {alexState.commits} </p>
                        <p> Number of Unit Tests:  3 </p>
                        <p> Number of Closed Issues:  {alexState.issues} </p>
                    </Card>

                    <Card className='inner-about-card'>
                        <h3> John Powers </h3>

                        <img
                            src="johnpfp.png"
                            float="left"
                            width="50%"
                            height="50%"
                            backgroundSize="cover"
                            alt="picture of john"
                        />

                        <p> John is a Junior at UT Austin studying Computer Science and will be graduating in May 2023. His main role was front-end. </p>
                        <p> Number of Commits:  {johnState.commits} </p>
                        <p> Number of Unit Tests:  26 </p>
                        <p> Number of Closed Issues:  {johnState.issues} </p>
                    </Card>

                    <Card className='inner-about-card'>
                        <h3> Joshua Brown </h3>

                        <img
                            src="joshpfp.png"
                            float="left"
                            width="50%"
                            height="50%"
                            backgroundSize="cover"
                            alt="picture of josh"
                        />

                        <p> Joshua is a Junior at UT Austin studying Computer Science and will be graduating in May 2023. His main role was back-end. </p>
                        <p> Number of Commits:  {joshuaState.commits} </p>
                        <p> Number of Unit Tests:  10 </p>
                        <p> Number of Closed Issues:  {joshuaState.issues} </p>
                    </Card>

                    <Card className='inner-about-card'>
                        <h3> Justin Leong </h3>

                        <img
                            src="justinpfp.png"
                            float="left"
                            width="50%"
                            height="50%"
                            backgroundSize="cover"
                            alt="picture of justin"
                        />

                        <p> Justin is a Senior at UT Austin studying Computer Science and will be graduating in December 2022. His main role was back-end. </p>
                        <p> Number of Commits:  {justinState.commits} </p>
                        <p> Number of Unit Tests: 15 </p>
                        <p> Number of Closed Issues:  {justinState.issues} </p>
                    </Card>

                    <Card className='inner-about-card'>
                        <h3> Sage Sanford </h3>

                        <img
                            src="sagepfp.png"
                            float="left"
                            width="50%"
                            height="50%"
                            backgroundSize="cover"
                            alt="picture of sage"
                        />

                        <p> Sage is a Junior at UT Austin studying Computer Science and will be graduating in May 2023. His main role was front-end. </p>
                        <p> Number of Commits:  {sageState.commits} </p>
                        <p> Number of Unit Tests:  26 </p>
                        <p> Number of Closed Issues:  {sageState.issues} </p>
                    </Card>
                </div>

            </Card>

            {/* FOURTH CARD */}
            <Card body className='main-about-card'>
                <h3>Phase Leaders</h3>
                <p>Phase 1: John Powers</p>
                <p>Phase 2: Alexander Han</p>
                <p>Phase 3: Sage Sanford</p>
                <p>Phase 4: Joshua Brown</p>
            </Card>

            {/* FIFTH CARD */}
            <Card body className='main-about-card'>
                <h3> API's Used</h3>

                <div className='inner-about-flex'>

                    <Card body className='inner-about-card'>
                        <h4>
                            <a href="https://www.nps.gov/subjects/developer/api-documentation.htm">
                                National Park Service API
                            </a>
                        </h4>
                        <p>
                            Used to get data and content from the National Park Service (NPS) about parks and their facilities, events, news, alerts, and more.
                        </p>
                    </Card>

                    <Card body className='inner-about-card'>
                        <h4>
                            <a href="https://explorer.natureserve.org/api-docs/">
                                NatureServe Explorer REST API
                            </a>
                        </h4>
                        <p>
                            Used to get information on animals and their datapoints.
                        </p>
                    </Card>

                    <Card body className='inner-about-card'>
                        <h4>
                            <a href="https://ridb.recreation.gov/docs">
                                Recreation Information Database (RIDB)
                            </a>
                        </h4>
                        <p>
                            Used as an authoritative source of information and services for federal lands, historic sites, museums, and other attractions/resources.
                        </p>
                    </Card>


                    <Card body className='inner-about-card'>
                        <h4>
                            <a href="https://apiv3.iucnredlist.org/api/v3/docs">
                                Red List API
                            </a>
                        </h4>
                        <p>
                            Used to get information on species and mainly where they are based by region.
                        </p>
                    </Card>

                </div>

            </Card>

            {/* SIXTH CARD */}
            <Card body className='main-about-card'>
                <h3> Tools Used</h3>

                <div className='inner-about-flex'>
                    <Card className='inner-about-card'>
                        <h3> <a href="https://reactjs.org/">React</a> </h3>

                        <img
                            src="logo192.png"
                            height="140"
                            width="140"
                            alt="picture of react logo"
                        />

                        <p> A front-end JavaScript library for building user interfaces or UI components. </p>
                        <p> Used to build our front-end. </p>
                    </Card>

                    <Card className='inner-about-card'>
                        <h3> <a href="https://www.postman.com/">Postman</a> </h3>

                        <img
                            src="postman.png"
                            height="140"
                            width="220"
                            alt="picture of postman logo"
                        />

                        <p> An API platform for building and using APIs. </p>
                        <p> Used to document our API. </p>
                    </Card>

                    <Card className='inner-about-card'>
                        <h3> <a href="https://reactjs.org/">React Bootstrap</a> </h3>

                        <img
                            src="bootstrap.png"
                            height="140"
                            width="140"
                            alt="picture of react bootstrap logo"
                        />

                        <p> A library for components that replaces Boostrap Javascript. </p>
                        <p> Used the components to build our website </p>
                    </Card>

                    <Card className='inner-about-card'>
                        <h3> <a href="https://reactjs.org/">Gitlab</a> </h3>

                        <img
                            src="gitlab.png"
                            height="140"
                            width="140"
                            alt="picture of gitlab logo"
                        />

                        <p> A DevOps software that combines the ability to develop, secure, and operate software in a single application. </p>
                        <p> Used as the repository to hold all the information on our project. </p>
                    </Card>

                    <Card className='inner-about-card'>
                        <h3> <a href="https://reactjs.org/">Amazon Web Services</a> </h3>

                        <img
                            src="aws.png"
                            height="140"
                            width="250"
                            alt="picture of amazon web services logo"
                        />

                        <p> A subsidiary of Amazon providing on-demand cloud computing platforms and APIs to individuals. </p>
                        <p> Used to host our website. </p>
                    </Card>

                    <Card className='inner-about-card'>
                        <h3> <a href="https://reactjs.org/">Namecheap</a> </h3>

                        <div>
                            <img
                                src="namecheap.png"
                                height="140"
                                width="140"
                                alt="picture of namecheap logo"
                            />
                        </div>

                        <p> A domain name registrar providing domain name registration and web hosting. </p>
                        <p> Used to get a domain for our website. </p>
                    </Card>
                </div>

            </Card>

            {/* SEVENTH CARD */}
            <Card body className='main-about-card'>
                <h3> Important Links </h3>
                <h4> <a href="https://gitlab.com/JohnPowow/animalwatch"> Gitlab </a> </h4>
                <h4> <a href="https://documenter.getpostman.com/view/15603846/UVsSMP42"> Postman </a> </h4>
            </Card>

        </div>
    )
}

export default About