import React from "react";
import { BrowserRouter } from "react-router-dom";

import NavbarComp from "../components/NavbarComp";
import Header from "../components/Header";
import App from "../App";
import About from "../pages/About";
import HomePage from "../pages/Homepage"
import Parks from "../pages/Parks"
import Recreation from "../pages/Recreation"
import Species from "../pages/Species"
import RecreationInstance from "../pages/RecreationInstance"
import SpeciesInstance from "../pages/SpeciesInstance"
import SearchPage from "../pages/SearchPage"

describe("Overall rendering tests", () => {

  // Test 1
  test("App renders without crashing", () => {
    <BrowserRouter>
      render(<App />);
      expect(screen.getByLabel('NavbarComp')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 2
  test("About Page renders without crashing", () => {
    <BrowserRouter>
      render(<About />);
      expect(screen.getByText('About us')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 3
  test("Home Page renders without crashing", () => {
    <BrowserRouter>
      render(<HomePage />);
      expect(screen.getByText('AnimalWatch')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 4
  test("Parks Page renders without crashing", () => {
    <BrowserRouter>
      render(<Parks />);
      expect(screen.getByText('Parks')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 5
  test("Parks Page Filter renders without crashing", () => {
    <BrowserRouter>
      render(<Parks />);
      expect(screen.getByText('State')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 6
  test("Parks Page Sort renders without crashing", () => {
    <BrowserRouter>
      render(<Parks />);
      expect(screen.getByText('Sort By')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 7
  test("Parks Page Search renders without crashing", () => {
    <BrowserRouter>
      render(<Parks />);
      expect(screen.getByText('Search')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 8
  test("Recreation Page renders without crashing", () => {
    <BrowserRouter>
      render(<Recreation />);
      expect(screen.getByText('Recreation')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 9
  test("Recreation Page Search renders without crashing", () => {
    <BrowserRouter>
      render(<Recreation />);
      expect(screen.getByText('Search')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 10
  test("Recreation Page Sort renders without crashing", () => {
    <BrowserRouter>
      render(<Recreation />);
      expect(screen.getByText('Sort By')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 11
  test("Recreation Page Filter renders without crashing", () => {
    <BrowserRouter>
      render(<Recreation />);
      expect(screen.getByText('Pets Allowed')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 12
  test("Species Page renders without crashing", () => {
    <BrowserRouter>
      render(<Species />);
      expect(screen.getByText('Species')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 13
  test("Species Page Search renders without crashing", () => {
    <BrowserRouter>
      render(<Species />);
      expect(screen.getByText('Search By')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 14
  test("Species Page Sort renders without crashing", () => {
    <BrowserRouter>
      render(<Species />);
      expect(screen.getByText('Sort By')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 15
  test("Species Page Filter renders without crashing", () => {
    <BrowserRouter>
      render(<Species />);
      expect(screen.getByText('Taxonomy Class')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 16
  test("SearchPage renders without crashing", () => {
    <BrowserRouter>
      render(<SearchPage />);
      expect(screen.getByText('Search for Information')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 17
  test("Recreation Page Filter renders without crashing", () => {
    <BrowserRouter>
      render(<Recreation />);
      expect(screen.getByText('Pets Allowed')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 18
  test("Species Page renders without crashing", () => {
    <BrowserRouter>
      render(<Species />);
      expect(screen.getByText('Species')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 19
  test("Species Page Search renders without crashing", () => {
    <BrowserRouter>
      render(<Species />);
      expect(screen.getByText('Search By')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 20
  test("Species Page Sort renders without crashing", () => {
    <BrowserRouter>
      render(<Species />);
      expect(screen.getByText('Sort By')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 21
  test("Species Page Filter renders without crashing", () => {
    <BrowserRouter>
      render(<Species />);
      expect(screen.getByText('Taxonomy Class')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 22
  test("Species Page Filter renders without crashing", () => {
    <BrowserRouter>
      render(<Species />);
      expect(screen.getByText('Taxonomy Class')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 23
  test("RecreationInstance Page renders without crashing", () => {
    <BrowserRouter>
      render(<RecreationInstance />);
      expect(screen.getByText('Recreation')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 24
  test("SpeciesInstance Page renders without crashing", () => {
    <BrowserRouter>
      render(<SpeciesInstance />);
      expect(screen.getByText('Species')).toBeInTheDocument();
    </BrowserRouter>;
  });

  // Test 25
  test("Navbar Rrenders without crashing", () => {
    <BrowserRouter>
      render(<NavbarComp />);
      expect(screen.getByText('Home')).toBeInTheDocument();
    </BrowserRouter>
  });

  // Test 26
  test("Header renders without crashing", () => {
    <BrowserRouter>
      render(<Header />);
      expect(screen.getByText('Header')).toBeInTheDocument();
    </BrowserRouter>
  });

});