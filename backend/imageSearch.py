
import requests
from bs4 import BeautifulSoup
import json
#

def get_images_links(searchTerm):

    searchUrl = "https://www.google.com/search?q={}&site=webhp&tbm=isch".format(searchTerm)
    d = requests.get(searchUrl).text
    soup = BeautifulSoup(d, 'html.parser')

    img_tags = soup.find_all('img')

    imgs_urls = []
    for img in img_tags:
        if img['src'].startswith("http"):
            imgs_urls.append(img['src'])
    print(imgs_urls)

    return(imgs_urls)

get_images_links('cat and dog')