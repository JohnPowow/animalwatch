from unicodedata import name
from flask import Flask, jsonify, request
import flask_restless
import flask_sqlalchemy
import sqlalchemy
# from deepdiff import DeepDiff
import difflib

import cred
from models import Species, Parks, Recreations, SpeciesAndParks, RecreationsAndSpecies, ParksAndRecreations

url = 'mysql://'+cred.USER + ':' + cred.PASSWORD + '@' + cred.ENDPOINT + cred.TABLE
# url = 'mysql://%s:%s@%s' % (USER, PASSWORD, HOST)

# Create the Flask application and the Flask-SQLAlchemy object.
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = url
db = flask_sqlalchemy.SQLAlchemy(app)


@app.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    # header['Access-Control-Allow-Methods'] = 'GET, POST, PATCH, PUT, DELETE, OPTIONS'

    header["Access-Control-Allow-Credentials"] = "true"
    header["Access-Control-Allow-Methods"] = "GET,HEAD,OPTIONS,POST,PUT"
    header["Access-Control-Allow-Headers"] = "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers"
    # Other headers can be added here if required
    return response


@app.route("/species/<int:id>", methods=["GET"])
def species_instance(id):
    species = db.session.query(Species).filter_by(id=id)[0]
    result = species.as_dict()

    # add parks from SpeciesAndParks table
    species_parks = db.session.query(SpeciesAndParks).filter_by(
        species=species.name).all()
    parks_list = []
    for sp in species_parks:
        park_id = db.session.query(Parks).filter_by(
            name=sp.park)[0].id
        parks_list.append({'id': park_id, 'name': sp.park})
    result['parks'] = parks_list

    # add recreations from RecreationsAndSpecies table
    recreations_species = db.session.query(RecreationsAndSpecies).filter_by(
        species=species.name).all()
    recreations_list = []
    for rs in recreations_species:
        recreation_id = db.session.query(Recreations).filter_by(
            name=rs.recreation)[0].id
        recreations_list.append({'id': recreation_id, 'name': rs.recreation})
    result['recreation'] = recreations_list

    result = jsonify(result)
    result.headers.add("Access-Control-Allow-Origin", "*")
    return result


@app.route("/parks/<int:id>", methods=["GET"])
def parks_instance(id):
    park = db.session.query(Parks).filter_by(id=id)[0]
    result = park.as_dict()

    # add species from SpeciesAndParks table
    species_parks = db.session.query(SpeciesAndParks).filter_by(
        park=park.name).all()
    species_list = []
    for sp in species_parks:
        species_id = db.session.query(Species).filter_by(
            name=sp.species)[0].id
        species_list.append({'id': species_id, 'name': sp.species})
    result['species'] = species_list

    # add recreations from RecreationsAndSpecies table
    parks_recreations = db.session.query(ParksAndRecreations).filter_by(
        park=park.name).all()
    recreations_list = []
    for pr in parks_recreations:
        recreation_id = db.session.query(Recreations).filter_by(
            name=pr.recreation)[0].id
        recreations_list.append({'id': recreation_id, 'name': pr.recreation})
    result['recreation'] = recreations_list

    result = jsonify(result)
    result.headers.add("Access-Control-Allow-Origin", "*")
    return result


@app.route("/recreation/<int:id>", methods=["GET"])
def recreation_instance(id):
    recreation = db.session.query(Recreations).filter_by(id=id)[0]
    result = recreation.as_dict()

    # add species from RecreationsAndSpecies table
    recreations_species = db.session.query(RecreationsAndSpecies).filter_by(
        recreation=recreation.name).all()
    species_list = []
    for rs in recreations_species:
        species_id = db.session.query(Species).filter_by(
            name=rs.species)[0].id
        species_list.append({'id': species_id, 'name': rs.species})
    result['species'] = species_list

    # add parks from ParksAndRecreations table
    parks_recreations = db.session.query(ParksAndRecreations).filter_by(
        recreation=recreation.name).all()
    parks_list = []
    for pr in parks_recreations:
        park_id = db.session.query(Parks).filter_by(
            name=pr.park)[0].id
        parks_list.append({'id': park_id, 'name': pr.park})
    result['parks'] = parks_list

    result = jsonify(result)
    result.headers.add("Access-Control-Allow-Origin", "*")
    return result


def get_start_end(queries, num_rows):
    items_per_page, start, end = 9, 0, num_rows

    # get starting and ending species, given a page number
    if queries.get('page'):
        page_num = int(queries['page'][0])

        start = (page_num-1) * items_per_page
        end = page_num * items_per_page
        if end > num_rows:
            end = num_rows
        if start > num_rows:
            start = num_rows

    return start, end

# iterate by filter, check specifi values


def filter_by_params(query, filter_params, data):
    for param in filter_params:
        print(param.key)
        if str(param.key) in data:
            query = query.filter(
                param == data[param.key])
    return query


def str_cmp(s1, s2):
    for a, b in zip(s1, s2):
        if a != b:
            print("diff \na:", a, "\nb:", b)


data = None


@app.route("/species", methods=["PUT"])
def species_data():
    data = request.json
    if data == None:
        data = {}


@app.route("/species", methods=["GET"])
def species_query():
    queries = request.args.to_dict(flat=False)
    species_query = Species.query
    # user passes in filters values

    # print('data type: ', data)
    dummy_data = {'tax_class': 'Aves'}

    # print('dummy data: ', dummy_data)
    # print('data type: ', str(type(data)))
    # print('dummy data type: ', str(type(dummy_data)))
    # print(type(Species.tax_class.key))
    # print(type(data[0]))

    # diff = DeepDiff(dummy_data, data)
    # print("diff!!!", diff)
    # assert(dummy_data.keys() == data.keys())
    # assert(dummy_data['tax_class'] == data['tax_class'])
    # assert(dummy_data == data)

    # data = dummy_data
    # asser
    # filter types
    filter_params = [Species.tax_class, Species.tax_phylum, Species.states]

    print("data      ::: ", str(data))
    print("dum_ data ::: ", str(dummy_data))

    print("diff ::", str_cmp(str(data), str(dummy_data)))

    i = 0
    # for i in range(len(str(data))):
    # print(i)
    # assert (str(data)[i] == str(dummy_data)[i])
    # species_query = filter_by_params(species_query, filter_params, data)
    for param in filter_params:
        # print(list(data.keys))
        if str(param.key) in data:
            # print("param :: ", param.key)
            # print(' data[param.key] = ', data[param.key])
            species_query = species_query.filter(
                param == str(data[param.key]))
    species_query = species_query.all()

    species_start, species_end = get_start_end(queries, len(species_query))

    result = {}
    result["count"] = species_end - species_start
    species_list = []
    # append relevant species
    for i in range(species_start, species_end):
        s = species_query[i]
        species_list.append(s.as_dict())
    result["page"] = species_list
    ret = jsonify(result)
    return ret


@app.route("/parks", methods=["GET"])
def parks_query():
    queries = request.args.to_dict(flat=False)
    all_parks = db.session.query(Parks).all()
    parks_start, parks_end = get_start_end(queries, len(all_parks))

    result = {}
    result["count"] = parks_end - parks_start
    parks_list = []
    # append relevant parks
    for i in range(parks_start, parks_end):
        s = all_parks[i]
        parks_list.append(s.as_dict())
    result["page"] = parks_list
    ret = jsonify(result)
    ret.headers.add("Access-Control-Allow-Origin", "*")
    return ret


@app.route("/recreation", methods=["GET"])
def recreations_query():
    queries = request.args.to_dict(flat=False)
    all_recreations = db.session.query(Recreations).all()
    recreations_start, recreations_end = get_start_end(
        queries, len(all_recreations))

    result = {}
    result["count"] = recreations_end - recreations_start
    recreations_list = []
    # append relevant recreations
    for i in range(recreations_start, recreations_end):
        s = all_recreations[i]
        recreations_list.append(s.as_dict())
    result["page"] = recreations_list
    ret = jsonify(result)
    ret.headers.add("Access-Control-Allow-Origin", "*")
    return ret


# Create the Flask-Restless API manager.
manager = flask_restless.APIManager(app, session=db.session)

# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well.
manager.create_api(Species, methods=['GET', 'POST', 'DELETE'])
# manager.create_api(Article, methods=['GET'])


# start the flask loop
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
