import requests
import json

from models import SpeciesStates


def addAllSpeciesStates(addFun):
    url = "https://explorer.natureserve.org/api/data/speciesSearch/"

    payload = json.dumps({
        "criteriaType": "species",
        "textCriteria": [],
        "statusCriteria": [],
        "locationCriteria": [
            {
                "paramType": "nation",
                "nation": "US"
            }
        ],
        "pagingOptions": {
            "page": None,
            "recordsPerPage": 2000
        },
        "recordSubtypeCriteria": [],
        "modifiedSince": None,
        "locationOptions": None,
        "classificationOptions": None,
        "speciesTaxonomyCriteria": [
            {
                "paramType": "scientificTaxonomy",
                "level": "KINGDOM",
                "scientificTaxonomy": "Animalia"
            }
        ]
    })
    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    species = response.json()['results']
    sql_id = 1
    for sp in species:
        name = sp['primaryCommonName']
        states = sp['nations'][0]['subnations']
        for state in states:
            abbrev = (state['subnationCode'])
            speciesStateObj = SpeciesStates(sql_id, name, str(abbrev))
            addFun(speciesStateObj)
            sql_id += 1
