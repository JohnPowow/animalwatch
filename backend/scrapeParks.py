import requests
import json
from bs4 import BeautifulSoup

from models import Species, Parks, Recreations, SpeciesAndParks, RecreationsAndSpecies, ParksAndRecreations


def addAllParks(addFun):
    url = "https://developer.nps.gov/api/v1/parks?limit=466&api_key=1Dwlbpu1NFUaPON2zzWx1JZ75tbg4QwVGF5qW1Vz"

    payload = {}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    parks = response.json()['data']
    ret = []
    sql_id = 1
    for park in parks:
        park_id = park['id']
        name = park['fullName']
        states = park['states']

        # Sometimes there's multiple admission costs.
        admissionCosts = ""
        costsList = []
        if(park['entranceFees']):
            for i in park['entranceFees']:
                costsList.append(int(float(i['cost'])))
        if(park['entrancePasses']):
            for j in park['entrancePasses']:
                costsList.append(int(float(j['cost'])))

        # format cost value
        if len(costsList) == 0:
            admissionCosts = "Free"
        else:
            min_cost = min(costsList)
            max_cost = max(costsList)
            if min_cost == max_cost:
                admissionCosts = "$" + str(max_cost)
            else:
                admissionCosts = "$" + str(min_cost) + ' - $' + str(max_cost)

        lat = park['latitude']
        lon = park['longitude']

        topics = "None"
        if len(park['topics']) > 0:
            topics = ""
            for t in park['topics']:
                topics += t['name'] + ', '
            topics = topics[:-2]

        activities = "None"
        if len(park['activities']) > 0:
            activities = ""
            for a in park['activities']:
                activities += a['name'] + ", "
            activities = activities[:-2]

        amenities = "None"
        parkcode = park['parkCode']

        url = "https://developer.nps.gov/api/v1/amenities/parksplaces?parkCode=" + \
            parkcode + "&api_key=1Dwlbpu1NFUaPON2zzWx1JZ75tbg4QwVGF5qW1Vz"
        am_response = requests.request(
            "GET", url, headers=headers, data=payload)
        am = am_response.json()['data']
        if len(am) > 0:
            amenities = ""
            for amenity in am:
                amenities += amenity[0]['name'] + ', '
        amenities = amenities[:-2]

        description = park['description']

        img_links = get_images_links(name)

        #id, name, states, year, admission_cost, latitude, longitude, topics, activities, amenities, description

        parkobj = Parks(sql_id, park_id, name, states, 'Unknown', admissionCosts,
                        lat, lon, topics, activities, amenities, description, img_links[0], img_links[1])
        sql_id += 1
        if sql_id % 100 == 0:
            print("id = ", sql_id)
        addFun(parkobj)


def get_images_links(searchTerm):

    searchUrl = "https://www.google.com/search?q={}&site=webhp&tbm=isch".format(
        searchTerm)
    d = requests.get(searchUrl).text
    soup = BeautifulSoup(d, 'html.parser')

    img_tags = soup.find_all('img')

    imgs_urls = []
    found = 0
    for img in img_tags:
        if found == 2:
            break
        if img['src'].startswith("http"):
            imgs_urls.append(img['src'])
            found += 1

    return(imgs_urls)
