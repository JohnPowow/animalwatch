import requests

from models import RecreationStates


def addAllRecreationStates(addFun):
    url = "https://ridb.recreation.gov/api/v1/facilities?limit=1000&offset=0&apikey=66b73af9-a66f-4d57-a18a-05b4cfa50414"

    payload = {}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    recreations = response.json()['RECDATA']
    sql_id = 1
    for recreation in recreations:
        recreation_id = recreation['FacilityID']
        name = recreation['FacilityName']

        payload = {}
        headers = {}

        # state
        url = "https://ridb.recreation.gov/api/v1/facilities/" + recreation_id + \
            "/facilityaddresses?apikey=66b73af9-a66f-4d57-a18a-05b4cfa50414"

        payload = {}
        headers = {}

        addResponse = requests.request(
            "GET", url, headers=headers, data=payload)
        addjson = addResponse.json()['RECDATA']

        state = None
        if addjson:
            state = addjson[0]['AddressStateCode']
        if state:
            # print(name, state)
            recStateObj = RecreationStates(sql_id, name, str(state))
            addFun(recStateObj)
            sql_id += 1
