import requests
import json
import re
from bs4 import BeautifulSoup

from models import Species, Parks, Recreations, SpeciesAndParks, RecreationsAndSpecies, ParksAndRecreations


def addAllSpecies(addFun):
    url = "https://explorer.natureserve.org/api/data/speciesSearch/"

    payload = json.dumps({
        "criteriaType": "species",
        "textCriteria": [],
        "statusCriteria": [],
        "locationCriteria": [
            {
                "paramType": "nation",
                "nation": "US"
            }
        ],
        "pagingOptions": {
            "page": None,
            "recordsPerPage": 10000
        },
        "recordSubtypeCriteria": [],
        "modifiedSince": None,
        "locationOptions": None,
        "classificationOptions": None,
        "speciesTaxonomyCriteria": [
            {
                "paramType": "scientificTaxonomy",
                "level": "KINGDOM",
                "scientificTaxonomy": "Animalia"
            }
        ]
    })
    headers = {
        'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)

    species = response.json()['results']
    sql_id = 1
    for sp in species:
        species_id = sp['elementGlobalId']
        name = sp['primaryCommonName']
        scientific_name = sp['scientificName']

        other_names_list = sp['speciesGlobal']['otherCommonNames']
        other_names = "None"
        if len(other_names_list) > 0:
            other_names = ""
            for n in other_names_list:
                other_names += n + ", "
            other_names = other_names[:-2]

        informal_taxonomy = sp['speciesGlobal']['informalTaxonomy']
        ecological_code = sp['elcode']
        last_updated = sp['lastModified']

        all_states = []
        states = sp['nations'][0]['subnations']
        for state in states:
            all_states.append(state['subnationCode'])

        states = "None"
        if len(all_states) > 0:
            states = ""
            for state in all_states:
                states += state + ", "
            states = states[:-2]

        tax_phylum = sp['speciesGlobal']['phylum']
        tax_class = sp['speciesGlobal']['taxclass']

        us_status = sp['speciesGlobal']['usesaCode']
        if not us_status:
            us_status = "None"

        global_status = sp['roundedGRank']

        # Need to find description
        uniqueId = sp['uniqueId']
        url = "https://explorer.natureserve.org/api/data/taxon/" + uniqueId

        payload = {}
        headers = {}

        descresponse = requests.request(
            "GET", url, headers=headers, data=payload)

        description = descresponse.json(
        )['speciesCharacteristics']['generalDescription']

        if description:
            # description = "Description not found"

            patterns = ['<>', '<.>', '<..>', '<...>', '<....>', '<.....>']
            for p in patterns:
                description = re.sub(p, '', description)
            description = re.sub('  ', ' ', description)

            img_links = get_images_links(name)
            speciesobj = Species(sql_id, species_id, name, scientific_name, other_names, informal_taxonomy, ecological_code,
                                 last_updated, states, tax_phylum, tax_class, us_status, global_status, description, img_links[0], img_links[1])
            sql_id += 1
            addFun(speciesobj)


def get_images_links(searchTerm):

    searchUrl = "https://www.google.com/search?q={}&site=webhp&tbm=isch".format(
        searchTerm)
    d = requests.get(searchUrl).text
    soup = BeautifulSoup(d, 'html.parser')

    img_tags = soup.find_all('img')

    imgs_urls = []
    found = 0
    for img in img_tags:
        if found == 2:
            break
        if img['src'].startswith("http"):
            imgs_urls.append(img['src'])
            found += 1

    return(imgs_urls)
