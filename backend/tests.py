import unittest
from unittest import TestCase
import requests

class UnitTest(TestCase):
    def test_species(self):
        endpoint = "https://api.animalwatch.net/species/1"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

    def test_recreation(self):
        endpoint = "https://api.animalwatch.net/recreation/1"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

    def test_parks(self):
        endpoint = "https://api.animalwatch.net/parks/1"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)
    
    def test_species_general(self):
        endpoint = "https://api.animalwatch.net/species"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

    def test_recreation_general(self):
        endpoint = "https://api.animalwatch.net/recreation"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)
        
    def test_parks_general(self):
        endpoint = "https://api.animalwatch.net/parks"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)
    ###Search, filter, sort
    def test_species_search(self):
        endpoint = "https://api.animalwatch.net/species?search=otter"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

    def test_species_filter(self):
        endpoint = "https://api.animalwatch.net/species?state=TX"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

    def test_species_sort(self):
        endpoint = "https://api.animalwatch.net/species?sort=name"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

    def test_recreation_search(self):
        endpoint = "https://api.animalwatch.net/recreation?search=north"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

    def test_recreation_filter(self):
        endpoint = "https://api.animalwatch.net/recreation?state=TX"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

    def test_recreation_sort(self):
        endpoint = "https://api.animalwatch.net/recreation?sort=name"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

    def test_parks_search(self):
        endpoint = "https://api.animalwatch.net/parks?search=north"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

    def test_parks_filter(self):
        endpoint = "https://api.animalwatch.net/parks?state=TX"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)
        
    def test_parks_sort(self):
        endpoint = "https://api.animalwatch.net/parks?sort=admission_cost"
        r = requests.get(endpoint)
        self.assertEqual(200, r.status_code)

if __name__ == '__main__':
    unittest.main()
