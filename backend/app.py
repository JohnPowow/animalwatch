from re import I
import re
from unicodedata import name
from flask import Flask, jsonify, request
import flask_restless
import flask_sqlalchemy
import sqlalchemy

import cred
from models import Species, Parks, Recreations, SpeciesAndParks, RecreationsAndSpecies, ParksAndRecreations

url = 'mysql://'+cred.USER + ':' + cred.PASSWORD + '@' + cred.ENDPOINT + cred.TABLE
# url = 'mysql://%s:%s@%s' % (USER, PASSWORD, HOST)

# Create the Flask application and the Flask-SQLAlchemy object.
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = url
db = flask_sqlalchemy.SQLAlchemy(app)


@app.after_request
def after_request(response):
    header = response.headers
    header['Access-Control-Allow-Origin'] = '*'
    # header['Access-Control-Allow-Methods'] = 'GET, POST, PATCH, PUT, DELETE, OPTIONS'

    header["Access-Control-Allow-Credentials"] = "true"
    header["Access-Control-Allow-Methods"] = "GET,HEAD,OPTIONS,POST,PUT"
    header["Access-Control-Allow-Headers"] = "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers"
    # Other headers can be added here if required
    return response


@app.route("/species/<int:id>", methods=["GET"])
def species_instance(id):
    species = db.session.query(Species).filter_by(id=id)[0]
    result = species.as_dict()

    # add parks from SpeciesAndParks table
    species_parks = db.session.query(SpeciesAndParks).filter_by(
        species=species.name).all()
    parks_list = []
    for sp in species_parks:
        filtered_parks = db.session.query(Parks).filter_by(
            name=sp.park).all()
        if len(filtered_parks) > 0:
            park_id = filtered_parks[0].id
            parks_list.append({'id': park_id, 'name': sp.park})
    result['parks'] = parks_list

    # add recreations from RecreationsAndSpecies table
    recreations_species = db.session.query(RecreationsAndSpecies).filter_by(
        species=species.name).all()
    recreations_list = []
    for rs in recreations_species:
        filtered_recreations = db.session.query(Recreations).filter_by(
            name=rs.recreation).all()
        if len(filtered_recreations) > 0:
            recreation_id = filtered_recreations[0].id
            recreations_list.append(
                {'id': recreation_id, 'name': rs.recreation})
    result['recreation'] = recreations_list

    result = jsonify(result)
    result.headers.add("Access-Control-Allow-Origin", "*")
    return result


@app.route("/parks/<int:id>", methods=["GET"])
def parks_instance(id):
    park = db.session.query(Parks).filter_by(id=id)[0]
    result = park.as_dict()

    # add species from SpeciesAndParks table
    species_parks = db.session.query(SpeciesAndParks).filter_by(
        park=park.name).all()
    species_list = []
    for sp in species_parks:
        filtered_species = db.session.query(Species).filter_by(
            name=sp.species).all()
        if len(filtered_species) > 0:
            species_id = filtered_species[0].id
            species_list.append({'id': species_id, 'name': sp.species})
        result['species'] = species_list

    # add recreations from RecreationsAndSpecies table
    parks_recreations = db.session.query(ParksAndRecreations).filter_by(
        park=park.name).all()
    recreations_list = []
    for pr in parks_recreations:
        filtered_recreations = db.session.query(Recreations).filter_by(
            name=pr.recreation).all()
        if len(filtered_recreations) > 0:
            recreation_id = filtered_recreations[0].id
            recreations_list.append(
                {'id': recreation_id, 'name': pr.recreation})
    result['recreation'] = recreations_list

    result = jsonify(result)
    result.headers.add("Access-Control-Allow-Origin", "*")
    return result


@app.route("/recreation/<int:id>", methods=["GET"])
def recreation_instance(id):
    recreation = db.session.query(Recreations).filter_by(id=id)[0]
    result = recreation.as_dict()

    # add species from RecreationsAndSpecies table
    recreations_species = db.session.query(RecreationsAndSpecies).filter_by(
        recreation=recreation.name).all()
    species_list = []
    for rs in recreations_species:
        filtered_species = db.session.query(Species).filter_by(
            name=rs.species).all()
        if len(filtered_species) > 0:
            species_id = filtered_species[0].id
            species_list.append({'id': species_id, 'name': rs.species})
    result['species'] = species_list

    # add parks from ParksAndRecreations table
    parks_recreations = db.session.query(ParksAndRecreations).filter_by(
        recreation=recreation.name).all()
    parks_list = []
    for pr in parks_recreations:
        filtered_parks = db.session.query(Parks).filter_by(
            name=pr.park).all()
        if len(filtered_parks) > 0:
            park_id = filtered_parks[0].id
            parks_list.append({'id': park_id, 'name': pr.park})
    result['parks'] = parks_list

    # result['num_species'] = len(species_list)

    result = jsonify(result)
    result.headers.add("Access-Control-Allow-Origin", "*")
    return result


def get_start_end(queries, num_rows):
    items_per_page, start, end = 9, 0, num_rows

    # get starting and ending species, given a page number
    if queries.get('page'):
        page_num = int(queries['page'][0])

        start = (page_num-1) * items_per_page
        end = page_num * items_per_page
        if end > num_rows:
            end = num_rows
        if start > num_rows:
            start = num_rows

    return start, end


@app.route("/species", methods=["GET"])
def species_query():
    # User passed-in values for arguments
    query_params = request.args.to_dict(flat=False)
    species_query = Species.query

    ## Filter ################
    filter_params = [Species.tax_class,
                     Species.tax_phylum, Species.states, Species.global_status]
    filtered_species_query = filter_by_params(
        query_params, species_query, filter_params)

    ## Search ################
    search_params = [Species.name, Species.scientific_name, Species.other_names,
                     Species.informal_taxonomy, Species.states, Species.last_updated, Species.tax_phylum,
                     Species.tax_class, Species.us_status, Species.global_status, Species.description]
    searched_query = filtered_species_query
    if 'search' in query_params:
        search_terms = query_params['search'][0]
        searched_query = search_by_terms(
            Species, filtered_species_query, search_params, search_terms)

    # return rows_by_page(searched_query.all(), 0, 1)

    ## Sort ##################
    if 'sort' in query_params:
        sort_params = [Species.name, Species.scientific_name, Species.other_names, Species.informal_taxonomy, Species.last_updated, Species.us_status, Species.global_status, Species.description, Species.tax_class,
                       Species.tax_phylum, Species.states, Species.global_status, ]
        col_name = query_params['sort'][0]
        for param in sort_params:
            if param.key == col_name:
                searched_query = searched_query.order_by(param)
                break

    result_rows = searched_query.all()
    species_start, species_end = get_start_end(query_params, len(result_rows))
    return rows_by_page(result_rows, species_start, species_end)


def search_by_terms(model, filtered_species_query, model_params, search_terms):
    search_terms = search_terms.split(' ')
    for i in range(len(search_terms)):
        search_terms[i] = re.sub(' ', '', search_terms[i])
    # remove empty strings
    search_terms = list(filter(lambda a: a != '', search_terms))

    # take a union of all filters of all attributes
    term_queries = [model.query.filter(False)
                    for _ in range(len(search_terms))]
    terms_rows = [None for _ in range(len(search_terms))]
    for i, term in enumerate(search_terms):
        for param in model_params:
            attribute_filter_query = filtered_species_query.filter(
                param.contains(term))

            term_queries[i] = term_queries[i].union(attribute_filter_query)

    for i in range(len(search_terms)):
        rows = term_queries[i].with_entities(model.id).all()
        terms_rows[i] = set([j[0] for j in rows])

    intersection_list = list(terms_rows[0].intersection(*terms_rows[1:]))
    return model.query.filter(model.id.in_(intersection_list))


def rows_by_page(filtered_model, model_start, model_end):
    result = {}
    result["count"] = model_end - model_start
    model_list = []
    # append relevant model
    for i in range(model_start, model_end):
        s = filtered_model[i]
        model_list.append(s.as_dict())
    result["page"] = model_list
    ret = jsonify(result)
    return ret


def filter_by_params(query_params, filtered_species, filter_params):
    # iterate by filter, check specific values
    for param in filter_params:
        if param.key in query_params:
            # CA,TX
            attribute = query_params[param.key]
            # filtered_species = filtered_species.filter(param == attribute)
            filtered_species = filtered_species.filter(
                param.contains(attribute))
    return filtered_species


@ app.route("/parks", methods=["GET"])
def parks_query():

    # User passed-in values for arguments
    query_params = request.args.to_dict(flat=False)
    parks_query = Parks.query

    ## Filter ################
    filter_params = [Parks.states]
    filtered_parks_query = filter_by_params(
        query_params, parks_query, filter_params)

    ## Search ################
    searched_query = filtered_parks_query
    search_params = [Parks.name, Parks.states, Parks.year, Parks.admission_cost, Parks.latitude,
                     Parks.longitude, Parks.topics, Parks.activities, Parks.amenities, Parks.description]
    if 'search' in query_params:
        search_terms = query_params['search'][0]
        searched_query = search_by_terms(
            Parks, filtered_parks_query, search_params, search_terms)

    ## Sort ##################
    if 'sort' in query_params:
        sort_params = [Parks.name, Parks.admission_cost,
                       Parks.latitude, Parks.longitude, Parks.num_species]
        col_name = query_params['sort'][0]
        for param in sort_params:
            if param.key == col_name:
                searched_query = searched_query.order_by(param)
                break

    result_rows = searched_query.all()
    parks_start, parks_end = get_start_end(query_params, len(result_rows))
    return rows_by_page(result_rows, parks_start, parks_end)


@ app.route("/recreation", methods=["GET"])
def recreations_query():
    # User passed-in values for arguments
    query_params = request.args.to_dict(flat=False)
    recreations_query = Recreations.query

    ## Filter ################
    filter_params = [Recreations.pets_allowed,
                     Recreations.reservable, Recreations.state]
    filtered_recreations_query = filter_by_params(
        query_params, recreations_query, filter_params)

    ## Search ################
    searched_query = filtered_recreations_query
    if 'search' in query_params:
        search_params = [Recreations.name, Recreations.link, Recreations.telephone, Recreations.activities,
                         Recreations.last_updated, Recreations.state, Recreations.coordinates, Recreations.description]
        search_terms = query_params['search'][0]
        searched_query = search_by_terms(Recreations,
                                         filtered_recreations_query, search_params, search_terms)

    ## Sort ##################
    if 'sort' in query_params:
        sort_params = [Recreations.name, Recreations.last_updated]
        col_name = query_params['sort'][0]
        for param in sort_params:
            if param.key == col_name:
                searched_query = searched_query.order_by(param)
                break

    result_rows = searched_query.all()
    recreations_start, recreations_end = get_start_end(
        query_params, len(result_rows))
    return rows_by_page(result_rows, recreations_start, recreations_end)


# Create the Flask-Restless API manager.
# manager = flask_restless.APIManager(app, session=db.session)

# Create API endpoints, which will be available at /api/<tablename> by
# default. Allowed HTTP methods can be specified as well.
# manager.create_api(Species, methods=['GET', 'POST', 'DELETE'])
# manager.create_api(Parks, methods=['GET', 'POST', 'DELETE'])
# manager.create_api(Article, methods=['GET'])


# start the flask loop
if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000, debug=True)
