from flask import Flask
import flask_sqlalchemy
import sqlalchemy
from scrapeParks import addAllParks
from scrapeRecreation import addAllRecreation
from scrapeRecreationStates import addAllRecreationStates
from scrapeSpeciesStates import addAllSpeciesStates
from scrapeSpecies import addAllSpecies
from models import Species

import cred

url = 'mysql://'+cred.USER + ':' + cred.PASSWORD + '@' + cred.ENDPOINT + cred.TABLE

# Create the Flask application and the Flask-SQLAlchemy object.
app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = url
db = flask_sqlalchemy.SQLAlchemy(app)
engine = sqlalchemy.create_engine(url)  # connect to server
create_str = "CREATE DATABASE IF NOT EXISTS data;"
engine.execute(create_str)
engine.execute("USE data;")


def addAll(model):
    db.session.add(model)
    db.session.commit()


# addAllParks(addAll)
# addAllSpecies(addAll)
addAllRecreation(addAll)
# addAllRecreationStates(addAll)
# addAllParksStates(addAll)
# addAllSpeciesStates(addAll))
