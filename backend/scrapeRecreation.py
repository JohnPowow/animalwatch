import requests
import re
from bs4 import BeautifulSoup

from models import Species, Parks, Recreations, SpeciesAndParks, RecreationsAndSpecies, ParksAndRecreations


def addAllRecreation(addFun):
    url = "https://ridb.recreation.gov/api/v1/facilities?limit=1000&offset=0&apikey=66b73af9-a66f-4d57-a18a-05b4cfa50414"

    payload = {}
    headers = {}
    response = requests.request("GET", url, headers=headers, data=payload)
    recreations = response.json()['RECDATA']
    sql_id = 1
    for recreation in recreations:
        recreation_id = recreation['FacilityID']
        name = recreation['FacilityName']

        # links
        url = "https://ridb.recreation.gov/api/v1/facilities/" + \
            recreation_id + "/links?apikey=66b73af9-a66f-4d57-a18a-05b4cfa50414"

        payload = {}
        headers = {}

        linkResponse = requests.request(
            "GET", url, headers=headers, data=payload)
        linkjson = linkResponse.json()['RECDATA']
        link = 'no link available'
        if linkjson:
            link = linkjson[0]['URL']

        telephone = recreation['FacilityPhone']
        if not telephone:
            telephone = "None"

        # activities
        url = "https://ridb.recreation.gov/api/v1/recareas/" + recreation_id + \
            "/activities?apikey=66b73af9-a66f-4d57-a18a-05b4cfa50414"

        payload = {}
        headers = {}

        actResponse = requests.request(
            "GET", url, headers=headers, data=payload)
        actjson = actResponse.json()['RECDATA']
        activities = "None"
        if len(actjson) > 0:
            activities = ""
            for a in actjson:
                activities += a['ActivityName'] + ', '
            activities = activities[:-2]

        lastUpdate = recreation['LastUpdatedDate']

        # state
        url = "https://ridb.recreation.gov/api/v1/facilities/" + recreation_id + \
            "/facilityaddresses?apikey=66b73af9-a66f-4d57-a18a-05b4cfa50414"

        payload = {}
        headers = {}

        addResponse = requests.request(
            "GET", url, headers=headers, data=payload)
        addjson = addResponse.json()['RECDATA']

        state = 'not available'
        if addjson:
            state = addjson[0]['AddressStateCode']

        coordinates_list = recreation['GEOJSON']['COORDINATES']
        coordinates = "Unknown"
        if coordinates_list:
            coordinates = ""
            for c in coordinates_list:
                coordinates += str(c) + ', '
            coordinates = coordinates[:-2]

        reservable = recreation['Reservable']
        if reservable:
            reservable = "Yes"
        else:
            reservable = "No"

        description = recreation['FacilityDescription']
        img_links = get_images_links(name)
        if description and len(img_links) == 2:
            # description = "Description not found"

            patterns = ['<>', '<.>', '<..>', '<...>', '<....>', '<.....>']
            for p in patterns:
                description = re.sub(p, '', description)
            description = re.sub('  ', ' ', description)

            # Need to find where pets allowed and mandatory equipment is
            recobj = Recreations(sql_id, recreation_id, name, link, telephone, activities, lastUpdate,
                                 state, coordinates, reservable, 'Yes', 'No', description, img_links[0], img_links[1])
            sql_id += 1
            addFun(recobj)


def get_images_links(searchTerm):

    searchUrl = "https://www.google.com/search?q={}&site=webhp&tbm=isch".format(
        searchTerm)
    d = requests.get(searchUrl).text
    soup = BeautifulSoup(d, 'html.parser')

    img_tags = soup.find_all('img')

    imgs_urls = []
    found = 0
    for img in img_tags:
        if found == 2:
            break
        if img['src'].startswith("http"):
            imgs_urls.append(img['src'])
            found += 1

    return(imgs_urls)
