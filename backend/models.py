from itertools import count
from re import S
import re
# import requests
# from bs4 import BeautifulSoup
from unicodedata import name
from flask import Flask, jsonify
import flask_restless
import flask_sqlalchemy
import sqlalchemy

import cred

url = 'mysql://'+cred.USER + ':' + cred.PASSWORD + '@' + cred.ENDPOINT + cred.TABLE

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = url
db = flask_sqlalchemy.SQLAlchemy(app)

# Base models


class Species(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    species_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(500), nullable=False)
    scientific_name = db.Column(db.String(500), nullable=False)
    other_names = db.Column(db.String(1000), nullable=False)
    informal_taxonomy = db.Column(db.String(200), nullable=False)
    states = db.Column(db.String(200), nullable=False)
    last_updated = db.Column(db.String(50), nullable=False)
    tax_phylum = db.Column(db.String(200))
    tax_class = db.Column(db.String(200))
    us_status = db.Column(db.String(50))
    global_status = db.Column(db.String(50))
    description = db.Column(db.String(9000), nullable=False)
    image_1 = db.Column(db.String(2000), nullable=False)
    image_2 = db.Column(db.String(2000), nullable=False)

    def __init__(self, id, species_id, name, scientific_name, other_names, informal_taxonomy,
                 ecological_code, last_updated, states, tax_phylum, tax_class,
                 us_status, global_status, description, image_1, image_2):
        self.id = id
        self.species_id = species_id
        self.name = name
        self.scientific_name = scientific_name
        self.other_names = other_names
        self.informal_taxonomy = informal_taxonomy
        self.ecological_code = ecological_code
        self.last_updated = last_updated
        self.states = states
        self.tax_phylum = tax_phylum
        self.tax_class = tax_class
        self.us_status = us_status
        self.global_status = global_status
        self.description = description
        self.image_1 = image_1
        self.image_2 = image_2

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Parks(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    park_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(500), nullable=False)
    states = db.Column(db.String(200), nullable=False)
    year = db.Column(db.String(50), nullable=False)
    admission_cost = db.Column(db.String(50), nullable=False)
    latitude = db.Column(db.Integer, nullable=False)
    longitude = db.Column(db.Integer, nullable=False)
    topics = db.Column(db.String(1000), nullable=False)
    activities = db.Column(db.String(1000), nullable=False)
    amenities = db.Column(db.String(1000), nullable=False)
    description = db.Column(db.String(5000), nullable=False)
    num_species = db.Column(db.Integer, nullable=False)
    image_1 = db.Column(db.String(2000), nullable=False)
    image_2 = db.Column(db.String(2000), nullable=False)

    def __init__(self, id, park_id, name, states, year, admission_cost,
                 latitude, longitude, topics, activities,
                 amenities, description, image_1, image_2, num_species):
        self.id = id
        self.park_id = park_id
        self.name = name
        self.states = states
        self.year = year
        self.admission_cost = admission_cost
        self.latitude = latitude
        self.longitude = longitude
        self.topics = topics
        self.activities = activities
        self.amenities = amenities
        self.description = description
        self.image_1 = image_1
        self.image_2 = image_2
        self.num_species = num_species

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Recreations(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    recreation_id = db.Column(db.Integer, nullable=False)
    name = db.Column(db.String(50), nullable=False)
    link = db.Column(db.String(50), nullable=False)
    telephone = db.Column(db.String(50), nullable=False)
    activities = db.Column(db.String(50), nullable=False)
    last_updated = db.Column(db.String(50), nullable=False)
    state = db.Column(db.String(50), nullable=False)
    coordinates = db.Column(db.String(50), nullable=False)
    reservable = db.Column(db.String(50), nullable=False)
    pets_allowed = db.Column(db.String(50), nullable=False)
    mandatory_equipment = db.Column(db.String(50), nullable=False)
    description = db.Column(db.String(5000), nullable=False)
    image_1 = db.Column(db.String(2000), nullable=False)
    image_2 = db.Column(db.String(2000), nullable=False)

    def __init__(self, id, recreation_id, name, link, telephone, activities,
                 last_updated, state, coordinates, reservable, pets_allowed,
                 mandatory_equipment, description, image_1, image_2):
        self.id = id
        self.recreation_id = recreation_id
        self.name = name
        self.link = link
        self.telephone = telephone
        self.activities = activities
        self.last_updated = last_updated
        self.state = state
        self.coordinates = coordinates
        self.reservable = reservable
        self.pets_allowed = pets_allowed
        self.mandatory_equipment = mandatory_equipment
        self.description = description
        self.image_1 = image_1
        self.image_2 = image_2

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

# Tables for scraping models and their respectie states


class SpeciesStates(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    species = db.Column(db.String(500), nullable=False)
    state = db.Column(db.String(2), nullable=False)

    def __init__(self, id, species, state):
        self.id = id
        self.species = species
        self.state = state


class ParkStates(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    park = db.Column(db.String(500), nullable=False)
    state = db.Column(db.String(2), nullable=False)

    def __init__(self, id, park, state):
        self.id = id
        self.park = park
        self.state = state


class RecreationStates(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    recreation = db.Column(db.String(500), nullable=False)
    state = db.Column(db.String(2), nullable=False)

    def __init__(self, id, recreation, state):
        self.id = id
        self.recreation = recreation
        self.state = state


class SpeciesAndParks(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    species = db.Column(db.String(50), nullable=False)
    park = db.Column(db.String(50), nullable=False)

    def __init__(self, id, species, park):
        self.id = id
        self.species = species
        self.park = park


class RecreationsAndSpecies(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    species = db.Column(db.String(50), nullable=False)
    recreation = db.Column(db.String(50), nullable=False)

    def __init__(self, id, species, recreation):
        self.id = id
        self.species = species
        self.recreation = recreation


class ParksAndRecreations(db.Model):
    id = db.Column(db.Integer, primary_key=True, nullable=False)
    park = db.Column(db.String(50), nullable=False)
    recreation = db.Column(db.String(50), nullable=False)

    def __init__(self, id, park, recreation):
        self.id = id
        self.park = park
        self.recreation = recreation


if __name__ == "__main__":
    db.create_all()

    states = ["AK", "AL", "AR", "AS", "AZ", "CA", "CO", "CT", "DC", "DE", "FL", "GA", "GU", "HI", "IA", "ID", "IL", "IN", "KS", "KY", "LA", "MA", "MD", "ME", "MI", "MN", "MO", "MP",
              "MS", "MT", "NC", "ND", "NE", "NH", "NJ", "NM", "NV", "NY", "OH", "OK", "OR", "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UM", "UT", "VA", "VI", "VT", "WA", "WI", "WV", "WY"]

    q = Species.query.filter(Species.tax_class == 'Amphibia')
    q = q.filter(Species.tax_phylum == 'Craniata')

    filtered_species_list = [
        s.name for s in q]

    # generate species filters and values dictionary
    def generate_species_attribute_vals():
        species_attributes_dict = {}

        tax_class_set = set()
        for tc in Species.query.distinct(Species.tax_class):
            tax_class_set.add(str(tc.tax_class))
        species_attributes_dict['tax_class'] = list(tax_class_set)

        tax_phylum_set = set()
        for tp in Species.query.distinct(Species.tax_phylum):
            tax_phylum_set.add(str(tp.tax_phylum))
        species_attributes_dict['tax_phylum'] = list(tax_phylum_set)

        global_status_set = set()
        for gs in Species.query.distinct(Species.global_status):
            global_status_set.add(str(gs.global_status))
        species_attributes_dict['global_status'] = list(global_status_set)

        species_attributes_dict['states'] = states
        print("species_attributes = ", species_attributes_dict)

    # generate species filters and values dictionary
    def generate_parks_attribute_vals():
        parks_attributes_dict = {}

        parks_attributes_dict['states'] = states
        print("parks_attributes = ", parks_attributes_dict)

    # generate species filters and values dictionary
    def generate_recreation_attribute_vals():
        recreation_attributes_dict = {}

        recreation_attributes_dict['pets_allowed'] = ['Yes', 'No']
        recreation_attributes_dict['reservable'] = ['Yes', 'No']
        recreation_attributes_dict['states'] = states

        print("recreation_attributes = ", recreation_attributes_dict)

    def generate_species_states():
        id = 0
        # print(Species.query.all()[0].states)
        for s in Species.query.all():
            # print("=====", s.states)
            s_states_string = re.sub(' ', '', s.states)
            s_states = s_states_string.split(',')
            for state in s_states:
                # print(state)
                speciesStateObj = SpeciesStates(id, s.name, state)
                db.session.add(speciesStateObj)
            db.session.commit()

    all_species_list = [s.name for s in Species.query.all()]
    animals_set = set(all_species_list)
    species_states_list = [s.species for s in SpeciesStates.query.all()]

    # print(len(all_species_list))
    # print(len(filtered_species_list))

    all_recreations_list = [r.name for r in Recreations.query.all()]
    recreations_set = set(all_recreations_list)
    recreations_states_list = [
        r.recreation for r in RecreationStates.query.all()]

    all_parks_list = [p.name for p in Parks.query.all()]
    parks_set = set(all_parks_list)
    parks_states_list = [p.park for p in ParkStates.query.all()]

    def populate_parks_and_recreations():
        id = 1
        for s in states:
            all_recreations = RecreationStates.query.filter_by(state=s).all()
            for recreation in all_recreations:
                all_parks = ParkStates.query.filter_by(state=s).all()
                for park in all_parks:
                    db.session.add(ParksAndRecreations(
                        id, park.park, recreation.recreation))
                    id += 1
                    if id % 500 == 0:
                        print(id)
            print('commit')
            db.session.commit()

    def populate_recreations_and_species():
        id = 1
        for s in states:
            all_recreations = RecreationStates.query.filter_by(state=s).all()
            for recreation in all_recreations:
                all_species = SpeciesStates.query.filter_by(state=s).all()
                for animal in all_species:
                    db.session.add(RecreationsAndSpecies(
                        id, animal.species, recreation.recreation))
                    id += 1
                    if id % 500 == 0:
                        print(id)
            print('commit')
            db.session.commit()

    # create relational table
    def populate_species_and_parks():
        id = 1
        for s in states:
            all_species = SpeciesStates.query.filter_by(state=s).all()
            for animal in all_species:
                all_parks = ParkStates.query.filter_by(state=s).all()
                for park in all_parks:
                    db.session.add(SpeciesAndParks(
                        id, animal.species, park.park))
                    id += 1
                    if id % 500 == 0:
                        print(id)
            db.session.commit()

    # def update_recreation_images():
    #     for i in range(1, len(Recreations.query.all())+1):
    #         instance_query = Recreations.query.filter(Recreations.id == i)
    #         links = get_images_links(instance_query.first().name)
    #         if len(links) == 2:
    #             instance_query.update(
    #                 {'image_1': links[0], 'image_2': links[1]})
    #         db.session.commit()

    # def get_images_links(searchTerm):

    #     searchUrl = "https://www.google.com/search?q={}&site=webhp&tbm=isch".format(
    #         searchTerm)
    #     d = requests.get(searchUrl).text
    #     soup = BeautifulSoup(d, 'html.parser')

    #     img_tags = soup.find_all('img')

    #     imgs_urls = []
    #     found = 0
    #     for img in img_tags:
    #         if found == 2:
    #             break
    #         if img['src'].startswith("http"):
    #             imgs_urls.append(img['src'])
    #             found += 1

    #     return(imgs_urls)

    # def update_recreation_reservable():
    #     for i in range(1, len(Recreations.query.all())+1):
    #         instance_query = Recreations.query.filter(Recreations.id == i)
    #         instance = instance_query.first()
    #         reservable = instance.reservable
    #         if reservable == 'Yes':
    #             reservable = 'No'
    #         else:
    #             reservable = 'Yes'

    #         instance_query.update({'reservable': reservable})
    #         if i % 50 == 0:
    #             print(i)
    #     db.session.commit()

    def update_park_num_species():
        for i in range(1, len(Parks.query.all())+1):
            instance_query = Parks.query.filter(Parks.id == i)
            instance = instance_query.first()

            park_species = db.session.query(
                SpeciesAndParks).filter_by(park=instance.name).all()
            instance_query.update({'num_species': len(park_species)})

            if i % 50 == 0:
                print(i)
            db.session.commit()

        db.session.commit()

    # update_park_num_species()
    # update_recreation_images()
    # generate_species_attribute_vals()
    # generate_parks_attribute_vals()
    # generate_recreation_attribute_vals()
    # generate_species_states()
    # populate_species_and_parks()
    # populate_recreations_and_species()
    # populate_parks_and_recreations()
